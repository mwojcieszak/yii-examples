<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 128]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 128]) ?>
    <?= $form->field($model, 'lock')->checkbox() ?>

    <?= $model->isNewRecord ? $form->field($model, 'password')->passwordInput(['maxlength' => 128]) : '' ?>

    <?= $form->field($model, 'roles')->dropDownList($model->getRoles(), ['multiple' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'CREATE'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'CANCEL'), ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
