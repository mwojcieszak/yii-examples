<?php
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\ModelSearch $searchModel
 */
$this->title = Yii::t('app/auth', 'USERS');
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);     ?>

    <p>
        <?= Html::a(Yii::t('app/auth', 'CREATE_USER', ['modelClass' => 'User']), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            'name',
            'last_name',
            'username',
            'email:email',
            [
                'header'        => Yii::t('app/auth', 'ROLE'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                return implode(', ', $model->getRolesInfo(true));
            }
            ],
            [
                'header'        => Yii::t('app/unit', 'EMPLOYER'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                if ($model->getCompany()) {
                    return $model->getCompany()->unit ? $model->getCompany()->unit->toInfo()['name'] : '-';
                } else {
                    return '-';
                }
            }
            ],
            [
                'attribute'     => 'lock',
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                return $model->lock ? Yii::t('app', 'YES') : Yii::t('app', 'NO');
            }
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'action-icon-tab',
                ],
                'template'       => '{view} {update} {regenerate-api-key} {delete}',
                'buttons'        => [
                    'view' => function ($url, $model) {
                    return Html::a(
                            '<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                            ]
                    );
                },
                    'update'         => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil icon-pencil"></span>', $url, [
                            'title'     => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                    ]);
                },
                    'regenerate-api-key' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-refresh icon-pencil"></span>', $url, [
                            'title'     => Yii::t('app/auth', 'REGENERATE_API_KEY'),
                            'data-pjax' => '0',
                    ]);
                },
                    'delete' => function ($url, $model) {
                    if ($model->id == Yii::$app->user->id) {
                        return false;
                    }

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title'        => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('app', 'DELETE_CONFIRMATION_MESSAGE'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                    ]);
                }
                ],
            ]
        ],
    ]);
    ?>

</div>
