<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 */
$this->title = $model->name;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'EDIT'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('app', 'DELETE'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('app/user', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ])
        ?>
        <?= Html::a(Yii::t('app', 'BACK'), ['index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'name',
            'last_name',
            'username',
            'email:email',
            'access_token',
            [
                'label' => Yii::t('app', 'CREATED_AT'),
                'name'  => 'created_at',
                'value' => date('d-m-Y H:i:s', $model->created_at),
            ],
            [
                'label' => Yii::t('app', 'UPDATED_AT'),
                'name'  => 'updated_at',
                'value' => date('d-m-Y H:i:s', $model->updated_at),
            ],
            [
                'label' => Yii::t('app/auth', 'ROLES'),
                'roles',
                'value' => implode(', ', $model->getRolesInfo(true))
            ]
        ],
    ])
    ?>

</div>
