<?php
use yii\grid\GridView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\ModelSearch $searchModel
 */
$this->title = Yii::t('app/project', 'ASSIGN_PROJECT_MEMBERS', $model->name);
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'layout'       => '{items}{pager}',
        'columns'      => [
            ['class' => 'yii\grid\CheckBoxColumn'],
            'name',
            'last_name',
            'username',
            'email:email'
        ],
    ]);
    ?>
</div>
