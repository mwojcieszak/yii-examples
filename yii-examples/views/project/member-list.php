<?php
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\ModelSearch $searchModel
 */
$this->title = Yii::t('app/project', 'CHOOSE_PROJECT_MEMBERS');
?>
<div class="user-index">
    <?php $form        = ActiveForm::begin(); ?>

    <?= $form->field($model, 'member_id')->dropDownList(ArrayHelper::map(User::getActive(), 'id', 'fullName'), ['multiple' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'CREATE') : Yii::t('app', 'UPDATE'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'CANCEL'), ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
