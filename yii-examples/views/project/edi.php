<?php
use app\models\Document;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectApprovalSettingSearchRules */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/project', 'PROJECTS');
?>
<div class="project-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('project.create')): ?>
        <p>
            <?= Html::a(Yii::t('app/project', 'CREATE_PROJECT', []), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            ['class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'action-icon-tab',
                ],
                'template'       => '{get-info}',
                'buttons'        => [
                    'get-info' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open icon-tab icon-middle"></span>', $url . '&type=' . Document::PROJECT_DOCUMENT, [
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                    ]);
                }
                ]
            ]
        ],
    ]);
    ?>
</div>
