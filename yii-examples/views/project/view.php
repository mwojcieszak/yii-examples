<?php
use app\models\Document;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */
?>
<div class="project-view">

    <h1><?= Html::encode($model->name) ?></h1>

    <p>
        <?php if (Yii::$app->user->can('project.update')): ?>
            <?= Html::a(Yii::t('app', 'EDIT'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can('project.delete')): ?>
            <?=
            Html::a(Yii::t('app', 'DELETE'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('app', 'DELETE_CONFIRMATION_MESSAGE'),
                    'method'  => 'post',
                ],
            ])
            ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('app', 'BACK'), ['index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/project', 'GET_XML'), ['get-info', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
        <?= Html::a(Yii::t('app/project', 'GET_EDI'), ['get-edi', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?=
    DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'name',
            [
                'label' => Yii::t('app/project', 'COMPANY_APPROVER'),
                'name'  => 'company_approver_id',
                'value' => $model->getCompany() ? $model->getCompany()->name : null,
            ],
            [
                'label' => Yii::t('app/project', 'INSTITUTION_APPROVER'),
                'name'  => 'institution_approver_id',
                'value' => $model->getOwner() ? $model->getOwner()->name : null,
            ],
            [
                'label' => Yii::t('app/project', 'LOCK_DATE'),
                'name'  => 'lock_date',
                'value' => $model->toInfo()['lock_date'],
            ],
        ],
    ])
    ?>
</div>
<div class="document-index">
    <?= Html::tag('div', '', ['id' => 'appletContainer']); ?>
    <?php $this->registerJs('
    jQuery("#loadButton").click(function () {
        if (typeof signer !== "object") {
            jQuery("#appletContainer").load("' . Url::to(['applet'], true) . '",{},function () {
                jQuery("#loadButton").remove();
                signer.initialize();
            });
        }
    });'); ?>
    <h1><?= Html::encode(Yii::t('app/project', 'DOCUMENT_LIST')) ?></h1>
    <?php if ($documentModel->canBeUploadedIntoProject()): ?>
        <?php
        Modal::begin([
            'id'           => 'modal',
            'toggleButton' => [
                'class' => 'btn btn-success modalButton',
                'label' => Yii::t('app/document', 'UPLOAD_PROJECT_DOCUMENT'),
            ],
        ]);
        ?>
        <div class='modalContent'>
            <?=
            $this->render('/document/_form', [
                'model' => $documentModel,
                'type'  => $type,
            ])
            ?>
        </div>
        <?php Modal::end(); ?>
    <?php endif; ?>

    <?php if ($documentModel->canBeUploadedIntoInspection()): ?>
        <?php
        Modal::begin([
            'id'           => 'modal',
            'toggleButton' => [
                'class' => 'btn btn-success modalButton',
                'label' => Yii::t('app/document', 'UPLOAD_INSPECTION_DOCUMENT'),
            ],
        ]);
        ?>
        <div class='modalContent'>
            <?=
            $this->render('/document/_form', [
                'model' => $documentModel,
                'type'  => $type,
            ])
            ?>
        </div>
        <?php Modal::end(); ?>
    <?php endif; ?>
    <?php if ($type == Document::INSPECTION_DOCUMENT): ?>
        <?=
        GridView::widget([
            'dataProvider' => $documentList,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'version',
                'created_at',
                [
                    'header'        => Yii::t('app/document', 'IS_APPROVED_BY_INSTITUTION'),
                    'format'        => 'html',
                    'enableSorting' => true,
                    'value'         => function ($model) {
                    return $model->getInstitutionApprover() ? Yii::t('app', 'YES') : Yii::t('app', 'NO');
                }
                ],
                [
                    'header'        => Yii::t('app/document', 'IS_SIGNED'),
                    'format'        => 'html',
                    'enableSorting' => true,
                    'value'         => function ($model) {
                    return Yii::t('app/document', $model->getSignatureType());
                }
                ],
                [
                    'header' => Yii::t('app', "SIGNATURE") . '<br />' . Html::button(Yii::t('app', 'LOAD_APPLET'), ['class' => 'btn btn-primary', 'id' => 'loadButton']),
                    'format' => 'raw',
                    'value'  => function ($model) {
                    if ($model->signature) {
                        return '<span class="glyphicon glyphicon-ok"></span>';
                    } else {
                        return Html::a('<span class="glyphicon glyphicon-barcode"></span>', '#', [
                                'title' => Yii::t('app', 'SIGN'),
                                'class' => 'signButton',
                                'style' => 'display: none;',
                                'data'  => [
                                    'task' => Url::to(['document/task', 'id' => $model->id], true),
                                ],
                        ]);
                    }
                }
                ],
                [
                    'class'      => 'yii\grid\ActionColumn',
                    'template'   => '{view}{update}{approve-document}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                    $params    = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = 'document/' . $action;

                    return Url::toRoute($params);
                },
                    'buttons' => [
                        'approve-document' => function ($url, $model) {
                        if ($model->isApprovedByType($model->type) || !Document::canApproveType($model->type)) {
                            return false;
                        }

                        return Html::a('<span class="glyphicon glyphicon-hand-up icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('app/document', 'APPROVE_DOCUMENT'),
                                'data-pjax' => '0',
                        ]);
                    },
                        'update' => function ($url, $model) {
                        if ($model->isApprovedByType($model->type) || !Document::canManageType($model->type)) {
                            return false;
                        }

                        return Html::a('<span class="glyphicon glyphicon-pencil icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('yii', 'Update'),
                                'data-pjax' => '0',
                        ]);
                    },
                        'delete' => function ($url, $model) {
                        if ($model->isApprovedByType($model->type) || !Document::canManageType($model->type)) {
                            return false;
                        }

                        return Html::a('<span class="glyphicon glyphicon-trash icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('yii', 'Delete'),
                                'data-pjax' => '0',
                        ]);
                    },
                    ]
                ],
            ]
        ]);
        ?>
    <?php elseif ($type == Document::PROJECT_DOCUMENT): ?>
        <?=
        GridView::widget([
            'dataProvider' => $documentList,
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                'version',
                'created_at',
                [
                    'header'        => Yii::t('app/document', 'IS_APPROVED_BY_COMPANY'),
                    'format'        => 'html',
                    'enableSorting' => true,
                    'value'         => function ($model) {
                    return $model->getCompanyApprover() ? Yii::t('app', 'YES') : Yii::t('app', 'NO');
                }
                ],
                [
                    'header'        => Yii::t('app/document', 'IS_SIGNED'),
                    'format'        => 'html',
                    'enableSorting' => true,
                    'value'         => function ($model) {
                    return Yii::t('app/document', $model->getSignatureType());
                }
                ],
                [
                    'header' => Yii::t('app', "SIGNATURE") . '<br />' . Html::button(Yii::t('app', 'LOAD_APPLET'), ['class' => 'btn btn-primary', 'id' => 'loadButton']),
                    'format' => 'raw',
                    'value'  => function ($model) {
                    if ($model->signature) {
                        return '<span class="glyphicon glyphicon-ok"></span>';
                    } else {
                        return Html::a('<span class="glyphicon glyphicon-barcode"></span>', '#', [
                                'title' => Yii::t('app', 'SIGN'),
                                'class' => 'signButton',
                                'style' => 'display: none;',
                                'data'  => [
                                    'task' => Url::to(['document/task', 'id' => $model->id], true),
                                ],
                        ]);
                    }
                }
                ],
                [
                    'class'      => 'yii\grid\ActionColumn',
                    'template'   => '{view}{approve-document}',
                    'urlCreator' => function ($action, $model, $key, $index) {
                    $params    = is_array($key) ? $key : ['id' => (string) $key];
                    $params[0] = 'document/' . $action;

                    return Url::toRoute($params);
                },
                    'buttons' => [
                        'approve-document' => function ($url, $model) {
                        if ($model->isApprovedByType($model->type) || !Document::canApproveType($model->type)) {
                            return false;
                        }

                        return Html::a('<span class="glyphicon glyphicon-hand-up icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('app/document', 'APPROVE_DOCUMENT'),
                                'data-pjax' => '0',
                        ]);
                    },
                        'delete' => function ($url, $model) {
                        if ($model->isApprovedByType($model->type) || !Document::canManageType($model->type)) {
                            return false;
                        }

                        return Html::a('<span class="glyphicon glyphicon-trash icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('yii', 'Delete'),
                                'data-pjax' => '0',
                        ]);
                    },
                    ]
                ],
            ]
        ]);
        ?>
    <?php endif; ?>
</div>
