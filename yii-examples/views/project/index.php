<?php
use app\models\Document;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectApprovalSettingSearchRules */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/project', 'PROJECTS');
?>
<div class="project-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (Yii::$app->user->can('project.create')): ?>
        <p>
            <?= Html::a(Yii::t('app/project', 'CREATE_PROJECT', []), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            [
                'header'        => Yii::t('app/unit', 'COMPANY'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                if ($company = $model->getCompany()) {
                    return $company->name;
                } else {
                    return '';
                }
            }
            ],
            [
                'header'        => Yii::t('app/project', 'EXTERNAL_DATA'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                return Html::a('www', '/external-data?ExternalDataSearch[project_id]=' . $model->id, []);
            }
            ],
            ['class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'action-icon-tab',
                ],
                'template'       => '{view}{update}{member-list}{process}{add-to-archive}{delete}',
                'buttons'        => [
                    'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open icon-tab icon-middle"></span>', $url . '&type=' . Document::PROJECT_DOCUMENT, [
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                    ]);
                },
                    'update'         => function ($url, $model) {
                    if (!Yii::$app->user->can('project.update')) {
                        return false;
                    }

                    return Html::a('<span class="glyphicon glyphicon-pencil icon-tab icon-middle"></span>', $url, [
                            'title'     => Yii::t('yii', 'Update'),
                            'data-pjax' => '0',
                    ]);
                },
                    'process' => function ($url, $model) {
                    if (!Yii::$app->user->can('project.process.view')) {
                        return false;
                    }

                    return Html::a('<span class="glyphicon glyphicon-map-marker icon-middle"></span>', $url, [
                            'title' => Yii::t('app/project', 'SET_PROCESS'),
                    ]);
                },
                    'add-to-archive' => function ($url, $model) {
                    if (!Yii::$app->user->can('project.archive.add')) {
                        return false;
                    }

                    if (!$model->isLocked()) {
                        return false;
                    }

                    return Html::a('<span class="glyphicon glyphicon-cloud icon-middle"></span>', $url, [
                            'title'        => Yii::t('app/project', 'ADD_TO_ARCHIVE'),
                            'data-confirm' => Yii::t('app', 'ARCHIVE_CONFIRMATION'),
                    ]);
                },
                    'delete' => function ($url, $model) {
                    if (!Yii::$app->user->can('project.delete')) {
                        return false;
                    }

                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title'        => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('app', 'DELETE_CONFIRMATION_MESSAGE'),
                            'data-method'  => 'post',
                            'data-pjax'    => '0',
                    ]);
                },
                ]
            ]
        ],
    ]);
    ?>
</div>
