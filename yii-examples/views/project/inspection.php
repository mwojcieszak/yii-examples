<?php
use app\models\Document;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectApprovalSettingSearchRules */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/project', 'INSPECTIONS');
?>
<div class="project-approval-rules-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            [
                'header'        => Yii::t('app/unit', 'COMPANY'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                if ($company = $model->getCompany()) {
                    return $company->name;
                } else {
                    return '';
                }
            }
            ],
            [
                'header'        => Yii::t('app/project', 'LOCK_DATE'),
                'format'        => 'html',
                'enableSorting' => true,
                'value'         => function ($model) {
                return sprintf("%s<br/>%s<br/>%s", $model->toInfo()['lock_date'], $model->toInfo()['company_approver'], $model->toInfo()['institution_approver']);
            }
            ],
            ['class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'action-icon-tab',
                ],
                'template'       => '{view}{approve-project}',
                'buttons'        => [
                    'approve-project' => function ($url, $model) {

                    if (!$model->isLocked() && (Yii::$app->user->can('projects.approve') || Yii::$app->user->can('inspections.approve'))) {
                        return Html::a('<span class="glyphicon glyphicon-flag icon-tab icon-middle"></span>', $url, [
                                'title'     => Yii::t('app/project', 'APPROVE_PROJECT'),
                                'data-pjax' => '0',
                        ]);
                    } else {
                        return false;
                    }
                },
                    'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open icon-tab icon-middle"></span>', $url . '&type=' . Document::INSPECTION_DOCUMENT, [
                            'title'     => Yii::t('yii', 'View'),
                            'data-pjax' => '0',
                    ]);
                },
                ]
            ]
        ],
    ]);
    ?>
</div>
