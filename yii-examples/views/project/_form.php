<?php
use app\models\Unit;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 64]) ?>

    <?php if (Yii::$app->user->can('project.admin')): ?>
        <?= $form->field($model, 'owner_id')->dropDownList(ArrayHelper::map(Unit::getResearchUnits(), 'id', 'name')) ?>
    <?php endif; ?>

    <?php if (!Yii::$app->user->can('project.view.company')): ?>
        <?= $form->field($model, 'company_id')->dropDownList(ArrayHelper::map(Unit::getCompanies(), 'id', 'name'), ['prompt' => '']) ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'CREATE'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'CANCEL'), ['index'], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
