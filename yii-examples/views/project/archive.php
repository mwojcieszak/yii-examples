<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectApprovalSettingSearchRules */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/project', 'ARCHIVE');
?>
<div class="project-approval-rules-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            'name',
            ['class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'action-icon-tab',
                ],
                'template'       => '{download}',
                'buttons'        => [
                    'download' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-download icon-tab icon-middle"></span>', $url, [
                            'title'     => Yii::t('app', 'DOWNLOAD'),
                            'data-pjax' => '0',
                    ]);
                },
                ]
            ]
        ],
    ]);
    ?>
</div>
