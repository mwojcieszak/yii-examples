<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectApprovalRules */

$this->title = Yii::t('app/project', 'UPDATE_PROJECT', [
        'modelClass' => 'Project Approval Rules',
    ]) . ': ' . $model->name;
?>
<div class="project-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
