<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Document */

$this->title = Yii::t('app/document', 'CREATE_DOCUMENT', [
        'modelClass' => 'Document',
    ]);
?>
<div class="document-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
