<?php
use app\assets\KirAsset;
use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

$kir = KirAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Document */
?>
<div class="document-view">
    <h1><?= Html::encode($model->title . " (wersja {$model->version})") ?></h1>
    <p>
        <?= Html::a(Yii::t('app', 'BACK'), ['project/view', 'id' => $model->project_id, 'type' => $model->type], ['class' => 'btn btn-primary']) ?>
        <?php if ($model->canManageType($model->type)): ?>
            <?=
            DetailView::widget([
                'model'      => $model,
                'attributes' => [
                    'name',
                    'description:ntext',
                ],
            ])
            ?>
            <?php
            Modal::begin([
                'id'           => 'modal',
                'toggleButton' => [
                    'class' => 'btn btn-success modalButton',
                    'label' => Yii::t('app/document', 'UPLOAD_NEW_VERSION'),
                ],
            ]);
            ?>
        <div class='modalContent'>
            <?=
            $this->render('/document/_form', [
                'model' => $documentModel,
            ])
            ?>
        </div>
        <?php Modal::end(); ?>
    <?php endif; ?>
</p>


<div class="comment-index">
    <?= Html::tag('div', '', ['id' => 'appletContainer']); ?>
    <?php $this->registerJs('
    jQuery("#loadButton").click(function () {
        if (typeof signer !== "object") {
            jQuery("#appletContainer").load("' . Url::to(['applet'], true) . '",{},function () {
                jQuery("#loadButton").remove();
                signer.initialize();
            });
        }
    });'); ?>
    <h2><?= Html::encode(Yii::t('app/comment', "COMMENT_LIST")) ?></h2>
    <?=
    GridView::widget([
        'dataProvider' => $commentList,
        'columns'      => [
            'created_at:datetime',
            [
                'attribute' => 'author',
                'header'    => Yii::t('app/comment', "AUTHOR"),
                'format'    => 'html',
                'value'     => function ($model) {
                if ($model->author) {
                    return $model->author->name;
                }

                return '<s>' . $model->getArchiveAuthor()['name'] . '</s>';
            }
            ],
            'doc_version',
            [
                'attribute' => 'content',
                'format'    => 'ntext',
                'value'     => function ($model) {
                return $model->content
                    . ($model->info_attachment ? ' /zał.: ' . $model->info_attachment : '')
                    . ($model->info_page ? ' /str.: ' . $model->info_page : '')
                    . ($model->info_paragraph ? ' /par.: ' . $model->info_paragraph : '')
                ;
            }
            ],
            [
                'header' => Yii::t('app', "SIGNATURE") . '<br />' . Html::button(Yii::t('app', 'LOAD_APPLET'), ['class' => 'btn btn-primary', 'id' => 'loadButton']),
                'format' => 'raw',
                'value'  => function ($model) {
                if ($model->signature) {
                    return '<span class="glyphicon glyphicon-ok"></span>';
                } else {
                    return Html::a('<span class="glyphicon glyphicon-barcode"></span>', '#', [
                            'title' => Yii::t('app', 'SIGN'),
                            'class' => 'signButton',
                            'style' => 'display: none;',
                            'data'  => [
                                'task' => Url::to(['comment/task', 'id' => $model->id], true),
                            ],
                    ]);
                }
            }
            ],
        ],
    ]);
    ?>
</div>

<div class="comment-form">
    <h3><?= Html::encode(Yii::t('app/comment', "ADD_COMMENT")) ?></h3>
    <?php
    $form = ActiveForm::begin([
            'action' => ['comment/create'],
    ]);
    ?>

    <?= $form->field($newComment, 'content')->textarea(['rows' => 6]) ?>
    <?= $form->field($newComment, 'info_attachment')->textInput() ?>
    <?= $form->field($newComment, 'info_page')->textInput() ?>
    <?= $form->field($newComment, 'info_paragraph')->textInput() ?>

    <?= Html::activeHiddenInput($newComment, 'project_id') ?>
    <?= Html::activeHiddenInput($newComment, 'document_id') ?>
    <?= Html::activeHiddenInput($newComment, 'doc_version') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'CREATE'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="document-index">
    <h1><?= Html::encode(Yii::t('app/document', 'ALL_VERSIONS')) ?></h1>
    <?=
    GridView::widget([
        'dataProvider' => $previousVerions,
        'columns'      => [
            'version',
            [
                'attribute' => 'title',
                'format'    => 'html',
                'value'     => function ($model) {
                return Html::a($model->title, Yii::$app->urlManager->createUrl(['document/download', 'id' => $model->id, 'version' => $model->version]));
            }
            ],
            'created_at',
        ],
    ]);
    ?>
</div>

</div>
