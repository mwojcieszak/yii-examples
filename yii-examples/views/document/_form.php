<?php
use app\models\Document;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Document */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="document-form">

    <?php
    $form = ActiveForm::begin([
            'action'  => [$model->isNewRecord ? 'document/create' : 'document/update', 'id' => $model->id, 'type' => $model->type, 'version' => $model->version],
            'options' => ['enctype' => 'multipart/form-data'],
    ]);
    ?>

    <?= Html::activeHiddenInput($model, 'project_id') ?>
    <?= $model->version ? Html::activeHiddenInput($model, 'version') : '' ?>
    <?= $model->id ? Html::activeHiddenInput($model, 'id') : '' ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 256]) ?>

    <?= $model->isNewRecord ? $form->field($model, 'file')->fileInput(['class' => 'btn btn-warning']): Html::activeHiddenInput($model, 'file') ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?=
    $form->field($model, 'signature_type')->dropDownList([
        Document::NO_SIGNATURE     => Yii::t('app/document', 'UNSIGNED'),
        Document::SIGNED_BY_HAND   => Yii::t('app/document', 'SIGNED_BY_HAND'),
        Document::SIGNED_DIGITALLY => Yii::t('app/document', 'SIGNED_DIGITALLY'),
    ])
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'CREATE'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'CANCEL'), ['project/view', 'id' => $model->project_id, 'type' => $model->type], ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
