<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Document */

$this->title = Yii::t('app/document', 'UPDATE_DOCUMENT', [
        'modelClass' => 'Document',
    ]);
?>
<div class="document-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
