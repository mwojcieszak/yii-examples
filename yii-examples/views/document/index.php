<?php
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DocumentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app/document', 'DOCUMENTS');
?>
<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'created_at',
            'updated_at',
            'project_id',
            'title',
            'size',
            ['class'    => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'],
        ],
    ]);
    ?>

</div>
