<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "project_processes".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $drug_name
 * @property string $drug_code
 * @property string $quantity
 * @property string $feeding_start
 * @property string $feeding_interval
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Projects $project
 */
class ProjectProcess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_processes';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'drug_name', 'drug_code', 'quantity', 'feeding_start', 'feeding_interval'], 'required'],
            [['project_id', 'created_at', 'updated_at'], 'integer'],
            [['feeding_interval'], 'double'],
            [['drug_name', 'drug_code', 'quantity', 'feeding_start'], 'string', 'max' => 128]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('app/project', 'ID'),
            'drug_name'        => Yii::t('app/project', 'DRUG_NAME'),
            'drug_code'        => Yii::t('app/project', 'DRUG_CODE'),
            'quantity'         => Yii::t('app/project', 'DRUG_QUANTITY'),
            'feeding_start'    => Yii::t('app/project', 'FEEDING_DATE'),
            'feeding_interval' => Yii::t('app/project', 'FEEDING_INTERVAL'),
            'created_at'       => Yii::t('app', 'CREATED_AT'),
            'updated_at'       => Yii::t('app', 'UPDATED_AT'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    public static function getDrugIntervals($id)
    {
        $results = [];

        foreach (self::findAll(['project_id' => $id]) as $drug) {
            $results[$drug->drug_code] = $drug->feeding_interval;
        }

        return $results;
    }

    public static function hasValidInterval($drugInterval, $pharmacyInterval)
    {
        return $drugInterval == $pharmacyInterval;
    }

    /*
     * Process interval from hoursto minutes
     */
    private static function processInterval($value)
    {
        return $value * 60;
    }

}
