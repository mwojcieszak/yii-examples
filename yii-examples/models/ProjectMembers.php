<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_members".
 *
 * @property integer $project_id
 * @property integer $member_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $member
 * @property Projects $project
 */
class ProjectMembers extends \yii\db\ActiveRecord
{
    private $project_it;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'member_id'], 'required'],
            [['project_id', 'member_id', 'created_at', 'updated_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app/project', 'PROJECT_ID'),
            'member_id'  => Yii::t('app/project', 'MEMBER_ID'),
            'created_at' => Yii::t('app/project', 'CREATED_AT'),
            'updated_at' => Yii::t('app', 'UPDATED_AT'),
        ];
    }

    public function getMembers()
    {
        return self::findAll(['project_id' => $this->project_id]);
    }

    public function getMemberIds()
    {
        $members = $this->getMembers();
        $result  = [];

        foreach ($members as $member) {
            $result[] = $member->member_id;
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    public function removeMembers()
    {
        foreach ($this->getMembers() as $member) {
            $member->delete();
        }

        return true;
    }

    public function saveMembers($members = [])
    {
        $transaction = $this->getDb()->beginTransaction();

        if (!$this->removeMembers()) {
            return false;
        }

        foreach ($members['member_id'] as $key => $val) {
            $member             = new ProjectMembers();
            $member->member_id  = $val;
            $member->project_id = $this->project_id;
            $member->save();
        }

        $transaction->commit();

        return true;
    }

}
