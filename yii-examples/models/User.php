<?php

namespace app\models;

use Yii;
use lib\behaviors\SoftDeleteBehavior;
use lib\helpers\ResetPasswordCrypter;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_ACTIVE = 1;

    public $roles = [];

    public function __toString()
    {
        return "{$this->id}\t{$this->username}";
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDelete' => ['class'     => SoftDeleteBehavior::className(),
                'attribute' => 'deleted_at',
                'timestamp' => time(),
                'safeMode'  => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'created_at',
                    'updated_at',
                    'hidden',
                    'lock',
                ],
                'integer',
            ],
            [
                [
                    'name'
                ],
                'string',
                'max' => 64
            ],
            [
                [
                    'username',
                    'password',
                    'email',
                    'roles',
                ],
                'required'
            ],
            [
                'email',
                'email'
            ],
            [
                'email',
                'unique',
                'targetAttribute' => 'email'
            ],
            [
                'username',
                'unique',
                'targetAttribute' => 'username'],
            [
                [
                    'last_name',
                    'username',
                    'password',
                    'auth_key',
                ],
                'string',
                'max' => 128,
            ],
            [
                [
                    'access_token'
                ],
                'string',
                'max' => 256,
            ],
            [
                [
                    'roles'
                ],
                'default'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'         => Yii::t('app/auth', 'NAME'),
            'last_name'    => Yii::t('app/auth', 'LASTNAME'),
            'username'     => Yii::t('app/auth', 'USERNAME'),
            'email'        => Yii::t('app/auth', 'EMAIL'),
            'password'     => Yii::t('app/auth', 'PASSWORD'),
            'created_at'   => Yii::t('app', 'CREATED_AT'),
            'updated_at'   => Yii::t('app', 'UPDATED_AT'),
            'roles'        => Yii::t('app/auth', 'ROLE'),
            'lock'         => Yii::t('app/auth', 'LOCK'),
            'access_token' => Yii::t('app/auth', 'ACCESS_TOKEN')
        ];
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function find()
    {
        $query = parent::find();

        return $query->where('deleted_at is null');
    }

    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];

        $parts     = ResetPasswordCrypter::decryptToken($token);
        $timestamp = (int) end($parts);

        if ($timestamp + $expire < time()) {
            return null;
        }

        return static::findOne([
                'id'        => $parts[0],
                'is_active' => self::STATUS_ACTIVE,
        ]);
    }

    public function canBeRemoved()
    {
        if (Yii::$app->user->id == $this->id) {
            return false;
        }

        if (!Yii::$app->user->identity->isSuperAdmin() && $this->isSuperAdmin()) {
            return false;
        }

        return true;
    }

    public function canBeEditedOrViewed()
    {
        if (!Yii::$app->user->identity->isSuperAdmin() && $this->isSuperAdmin()) {
            return false;
        }

        return true;
    }

    public function isSuperAdmin()
    {
        return Yii::$app->authManager->checkAccess($this->id, '*');
    }

    public static function getActive()
    {
        return self::findAll(['is_active' => true]);
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param  string  $authKey
     * @return boolean if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = sha1(Yii::$app->security->generateRandomKey());
                if (!$this->access_token) {
                    $this->access_token = sha1(Yii::$app->security->generateRandomKey());
                }
                $this->password  = Yii::$app->security->generatePasswordHash($this->password);
                $this->is_active = self::STATUS_ACTIVE;
            }

            return true;
        }

        return false;
    }

    public function regenerateApiKey()
    {
        $this->access_token = sha1(Yii::$app->security->generateRandomKey());

        $this->roles = $this->getRolesInfo();

        return $this->save();
    }

    public function afterSave($insert, $changedAttributes = [])
    {
        $this->appendRoles($this->roles ? $this->roles : $this->getRolesInfo());

        parent::afterSave($insert, $changedAttributes);
    }

    public function getRoles()
    {
        return AuthItem::getRolesInfo();
    }

    public function getRolesInfo($i18n = false)
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        if (!$roles) {
            return [];
        }

        $results = array_map(function ($roles, $i18n) {
            return $i18n ? Yii::t('app/roles', $roles->name) : $roles->name;
        }, $roles, [$i18n]);

        return $results;
    }

    public function generatePasswordResetToken()
    {
        return ResetPasswordCrypter::encryptId($this->id);
    }

    private function revokeRolesAll()
    {
        $roles = Yii::$app->authManager->getRolesByUser($this->id);

        foreach ($roles as $role) {
            Yii::$app->authManager->revoke($role, $this->id);
        }

        return count(Yii::$app->authManager->getAssignments($this->id)) == 0;
    }

    private function appendRoles(array $roles = [])
    {
        /*
         * @var $auth lib\rbac\DbManager
         */
        $auth = Yii::$app->authManager;

        $transaction = $this->getDb()->beginTransaction();

        if (!$this->revokeRolesAll()) {
            return false;
        }

        foreach ($roles as $role) {
            $data = [];
            if (is_array($role)) {
                $name = $role[0];
                $data = count($role) > 1 ? array_splice($role, 1) : [];
            } else {
                $name = $role;
            }
            $authItem = AuthItem::findOne([
                    'name' => $name,
                    'type' => AuthItem::AUTH_ROLE,
            ]);

            if ($authItem) {
                $auth->assign($authItem, $this->id, null, $data);
            }
        }
        $transaction->commit();
    }

    public function getNotificationCount($state)
    {
        return Notification::withUser(Yii::$app->user->id)->andWhere(['read' => $state])->count();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    public static function getEmployees($type)
    {
        $result = [];
        if ($type == UnitType::COMPANY) {
            $type = [AuthItem::EMPLOYEE_RULE, AuthItem::EMPLOYEE_HEAD_RULE];
        }

        if ($type == UnitType::RESEARCH_UNIT) {
            $type = [AuthItem::INSTITUTION_RULE, AuthItem::INSTITUTION_HEAD_RULE];
        }

        $employees = (new \yii\db\Query())
                ->from('user u')
                ->leftJoin('unit_employees ue', 'ue.employee_id = u.id')
                ->innerJoin('auth_assignment as', 'u.id = as.user_id')
                ->where('employee_id is null')
                ->andWhere(['as.item_name' => $type])
                ->andWhere(['is_active' => true])->all();

        foreach ($employees as $employee) {
            $result[$employee['id']]['full_name'] = sprintf('%s %s', $employee['name'], $employee['last_name']);
            $result[$employee['id']]['role']      = Yii::t('app/roles', $employee['item_name']);
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames()
    {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    public function getfullName()
    {
        return sprintf('%s %s', $this->name, $this->last_name);
    }

    public function getCompany()
    {
        return UnitEmployee::find()->innerJoin('units u', 'u.id = unit_id')
                ->where(['employee_id' => $this->id])
                ->andWhere('u.deleted_at is null')
                ->one();
    }

    public function addToUnits(array $units)
    {
        $return = [];
        foreach ($units as $u) {
            $ue = new UnitEmployee([
                'employee_id' => $this->id,
            ]);
            if (is_integer($u)) {
                $ue->unit_id = $u;
            } elseif (is_string($u)) {
                $ue->unit_id = Unit::findOne(['name' => $u])->id;
            } elseif (is_object($u)) {
                $ue->unit_id = $u->id;
            }
            if (!$ue->save()) {
                throw new Exception(print_r($ue->errors, true));
            }
            $return[] = "$ue";
        }

        return $return;
    }

}
