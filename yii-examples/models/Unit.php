<?php

namespace app\models;

use Yii;
use lib\behaviors\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "units".
 *
 * @property integer $id
 * @property string $name
 * @property string $street
 * @property string $city
 * @property string $postal_code
 * @property integer $nip
 * @property integer $regon
 * @property string $short_desritption
 * @property string $long_description
 * @property integer $type_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property UnitEmployees[] $unitEmployees
 * @property User[] $employees
 * @property UnitTypes $type
 */
class Unit extends \yii\db\ActiveRecord
{
    public static function find()
    {
        $query = parent::find();

        return $query->where('deleted_at is null');
    }

    public function __toString()
    {
        return "{$this->id}\t{$this->name}";
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDelete' => ['class'     => SoftDeleteBehavior::className(),
                'attribute' => 'deleted_at',
                'timestamp' => time(),
                'safeMode'  => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'units';
    }

    public static function getCompanies()
    {
        return self::findAll(['type_id' => UnitType::COMPANY]);
    }

    public static function getResearchUnits()
    {
        return self::findAll(['type_id' => UnitType::RESEARCH_UNIT]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'street', 'city', 'postal_code', 'nip', 'regon', 'type_id'], 'required'],
            [['nip', 'regon', 'type_id', 'created_at', 'updated_at'], 'integer'],
            [['long_description'], 'string'],
            [['name', 'street', 'short_desritption'], 'string', 'max' => 256],
            [['city'], 'string', 'max' => 128],
            [['postal_code'], 'string', 'max' => 20],
            [['nip'], 'unique'],
            [['regon'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => Yii::t('app/unit', 'ID'),
            'name'              => Yii::t('app/unit', 'NAME'),
            'street'            => Yii::t('app/unit', 'STREET'),
            'city'              => Yii::t('app/unit', 'CITY'),
            'postal_code'       => Yii::t('app/unit', 'POSTAL_CODE'),
            'nip'               => Yii::t('app/unit', 'NIP'),
            'regon'             => Yii::t('app/unit', 'REGON'),
            'short_desritption' => Yii::t('app/unit', 'SHORT_DESCRIPTION'),
            'long_description'  => Yii::t('app/unit', 'LONG_DESCRIPTION'),
            'type_id'           => Yii::t('app/unit', 'TYPE_ID'),
            'created_at'        => Yii::t('app', 'CREATED_AT'),
            'updated_at'        => Yii::t('app', 'UPDATED_AT'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitEmployees()
    {
        return $this->hasMany(UnitEmployee::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(User::className(), ['id' => 'employee_id'])->viaTable('unit_employees', ['unit_id' => 'id']);
    }

    public function getEmployesIds()
    {
        return ArrayHelper::getColumn($this->getEmployees()->all(), 'id');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(UnitType::className(), ['id' => 'type_id']);
    }

    public function saveEmployees($employees = [])
    {
        $transaction = $this->getDb()->beginTransaction();

        foreach ($employees['employee_id'] as $val) {
            $member              = new UnitEmployee();
            $member->unit_id     = $this->id;
            $member->employee_id = $val;

            if ($member->validate()) {
                $member->save();
            }
        }

        $transaction->commit();

        return true;
    }

    public function isCompany()
    {
        return $this->type_id === UnitType::COMPANY;
    }

    public function isResearchUnit()
    {
        return $this->type_id === UnitType::RESEARCH_UNIT;
    }

    public function toInfo()
    {
        return [
            'name' => $this->name
        ];
    }

    private function removeEmployees()
    {
        foreach ($this->getUnitEmployees()->all() as $employee) {
            $employee->delete();
        }

        return true;
    }

}
