<?php

namespace app\models;

use Yii;
use lib\helpers\Mailer;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 *
 * @property NotificationUser[] $notificationUsers
 * @property User[] $users
 */
class Notification extends ActiveRecord
{
    const NOTIFICATION_INTERNAL = 'internal';
    const NOTIFICATION_EXTERNAL = 'external';

    public function __construct($type = false)
    {
        if ($type) {
            $this->notification_type_id = NotificationType::findByName($type);
        }

        parent::__construct();
    }

    public function __set($property, $value)
    {
        if ($property == 'notification_type_id' && $value == null) {
            throw new \Exception("Object has no $property valid property");
        }

        parent::__set($property, $value);
    }

    public static function withUser($userId)
    {
        return (new \yii\db\Query())
                ->from('notification n')
                ->innerJoin('notification_user nu', 'n.id = nu.notification_id')
                ->where(['nu.user_id' => $userId])
                ->orderBy('read,n.id ASC');
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title'   => Yii::t('app/notification', 'TITLE'),
            'content' => Yii::t('app/notification', 'CONTENT'),
        ];
    }

    public function add($type, $data, $params)
    {
        switch ($type) {
            case self::NOTIFICATION_INTERNAL:
                return $this->setInternalNotification($data, $params);
            case self::NOTIFICATION_EXTERNAL:
                return $this->setExternalNotification($data, $params);
            default:
                return false;
        }
    }

    public function toInfo()
    {
        return ['title'   => $this->title,
            'content' => $this->content
        ];
    }

    /*
     * Notifications into user account
     */
    private function setInternalNotification($data, $params)
    {
        $this->load(['Notification' => $data]);

        if ($this->save()) {
            return $this->setRecipmentByPermission($params['permission'], isset($params['project']) ? $params['project'] : null, isset($params['unit']) ? $params['unit'] : null);
        }
    }

    /*
     * Notifactaion sends by email
     *
     */
    private function setExternalNotification($data, $params)
    {
        $recipments = $this->setInternalNotification($data, $params);

        foreach ($recipments as $recipment) {
            Mailer::sendNotification($recipment['email'], $this->toInfo());
        }
    }

    private function setRecipmentByPermission($permission, $projectId = null)
    {
        $recipments = AuthItemChild::getUserByPermissionOrRole($permission, $projectId);

        if (!$recipments) {
            return [];
        }

        foreach ($recipments as $recipment) {
            $notificationUser = new NotificationUser();

            $notificationUser->load([
                'NotificationUser' => [
                    'notification_id' => $this->id,
                    'user_id'         => $recipment['id'],
                ],
            ]);
            $notificationUser->save();
        }

        return $recipments;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'title',
                'notification_type_id'],
                'required'],
            [[
                'notification_type_id'],
                'integer'],
            [[
                'title'],
                'string',
                'max' => 64],
            [[
                'content'],
                'string',
                'max' => 512]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationType()
    {
        return $this->hasOne(NotificationType::className(), ['id' => 'notification_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationUsers()
    {
        return $this->hasMany(NotificationUser::className(), ['notification_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('notification_user', ['notification_id' => 'id']);
    }

}
