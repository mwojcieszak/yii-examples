<?php

namespace app\models;

use Yii;
use lib\behaviors\SoftDeleteBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "comments".
 *
 * @property integer $project_id
 * @property integer $document_id
 * @property string $content
 * @property string $info_attachment
 * @property string $info_page
 * @property string $info_paragraph
 * @property integer $author_id
 *
 * @property User $author
 * @property Documents $document
 * @property Projects $project
 */
class Comment extends ActiveRecord
{
    public static function find()
    {
        $query = parent::find();

        return $query->where('deleted_at is null');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDelete' => ['class'     => SoftDeleteBehavior::className(),
                'attribute' => 'deleted_at',
                'timestamp' => time(),
                'safeMode'  => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'project_id', 'document_id', 'author_id', 'doc_version'], 'integer'],
            [['project_id', 'document_id', 'content', 'author_id', 'doc_version'], 'required'],
            [['content'], 'string'],
            [['info_attachment', 'info_page', 'info_paragraph'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app/comment', 'ID'),
            'created_at'      => Yii::t('app', 'CREATED_AT'),
            'updated_at'      => Yii::t('app', 'UPDATED_AT'),
            'project_id'      => Yii::t('app/comment', 'PROJECT'),
            'document_id'     => Yii::t('app/comment', 'DOCUMENT'),
            'content'         => Yii::t('app/comment', 'CONTENT'),
            'info_attachment' => Yii::t('app/comment', 'INFO_ATTACHMENT'),
            'info_page'       => Yii::t('app/comment', 'INFO_PAGE'),
            'info_paragraph'  => Yii::t('app/comment', 'INFO_PARAGRAPH'),
            'author_id'       => Yii::t('app/comment', 'AUTHOT'),
            'doc_version'     => Yii::t('app/comment', 'DOC_VERSION'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    public function getArchiveAuthor()
    {
        return (new \yii\db\Query())
                ->from('user u')
                ->innerJoin('comments c', 'c.author_id = u.id')
                ->where(['c.author_id' => $this->author_id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getCryptoTask()
    {
        return [
            'SigningTask' => [
                'Format'          => 'XAdES-BES',
                'DataEmbedding'   => 'true',
                'SigningTaskItem' => [
                    '.Id'               => 'ID-' . sha1($this->id) . '-' . time(),
                    'DataToSign'        => [
                        'URI' => Url::to(['comment/xml', 'id' => $this->id], true),
                    ],
                    'SignatureToCreate' => [
                        'URI' => Url::to(['comment/signature', 'id' => $this->id], true),
                    ],
                ],
            ],
        ];
    }

}
