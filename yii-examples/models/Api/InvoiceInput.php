<?php

namespace app\models\Api;

use app\models\ExternalData;

class InvoiceInput extends ExternalDataInput
{
    public $name;
    public $invoiceNumber;
    public $link;
    public $price;
    public $tax;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['name', 'invoiceNumber', 'link', 'price', 'tax'], 'required'],
            [['name', 'invoiceNumber', 'link'], 'string'],
            [['price', 'tax'], 'double']
        ]);
    }

    public function toInfo()
    {
        return [
            'name'          => $this->name,
            'invoiceNumber' => $this->invoiceNumber,
            'link'          => $this->link,
            'price'         => $this->price,
            'tax'           => $this->tax
        ];
    }

    public function buildArray()
    {
        return [
            'project_id' => $this->projectId,
            'type_id'    => ExternalData::TYPE_INVOICE,
            'data'       => json_encode($this->toInfo()),
            'created_at' => time()
        ];
    }

}
