<?php

namespace app\models\Api;

use yii\base\Model;

class ExternalDataInput extends Model
{
    public $projectId;

    public function rules()
    {
        return [['projectId', 'integer']];
    }

    public function __construct($projectId)
    {
        $this->projectId = $projectId;

        parent::__construct();
    }

}
