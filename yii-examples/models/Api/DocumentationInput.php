<?php

namespace app\models\Api;

use app\models\ExternalData;

class DocumentationInput extends ExternalDataInput
{
    public $name;
    public $link;
    public $type;
    public $createdDate;

    public function rules()
    {
        return (array_merge(parent::rules(), [
                [['name', 'link', 'type', 'createdDate'], 'required'],
                [['createdDate'], 'date', 'format' => 'Y-m-d H:i:s'],
                [['name', 'link', 'type'], 'string']
        ]));
    }

    public function toInfo()
    {
        return [
            'name'        => $this->name,
            'link'        => $this->link,
            'type'        => $this->type,
            'createdDate' => $this->createdDate
        ];
    }

    public function buildArray()
    {
        return [
            'project_id' => $this->projectId,
            'type_id'    => ExternalData::TYPE_DOCUMENTATION,
            'data'       => json_encode($this->toInfo()),
            'created_at' => time()
        ];
    }

}
