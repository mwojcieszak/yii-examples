<?php

namespace app\models\Api;

use app\models\ExternalData;

class PersonnelInput extends ExternalDataInput
{
    public $fullName;
    public $priceWithTax;
    public $link;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['fullName', 'priceWithTax', 'link'], 'required'],
            [['fullName', 'link'], 'string'],
            [['priceWithTax'], 'double']
        ]);
    }

    public function toInfo()
    {
        return [
            'fullName'     => $this->fullName,
            'priceWithTax' => $this->priceWithTax,
            'link'         => $this->link
        ];
    }

    public function buildArray()
    {
        return [
            'project_id' => $this->projectId,
            'type_id'    => ExternalData::TYPE_PERSONNEL,
            'data'       => json_encode($this->toInfo()),
            'created_at' => time()
        ];
    }

}
