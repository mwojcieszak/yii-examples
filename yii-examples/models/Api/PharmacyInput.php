<?php

namespace app\models\Api;

use app\models\ExternalData;

class PharmacyInput extends ExternalDataInput
{
    public $drugCode;
    public $drugName;
    public $usageDate;
    public $patientCode;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['drugCode', 'drugName', 'usageDate', 'patientCode'], 'required'],
            [['usageDate'], 'date', 'format' => 'Y-m-d H:i:s'],
            [['drugCode', 'drugName', 'usageDate', 'patientCode'], 'string']
        ]);
    }

    public function toInfo()
    {
        return [
            'drug_code'    => $this->drugCode,
            'drug_name'    => $this->drugName,
            'usage_date'   => $this->usageDate,
            'patient_code' => $this->patientCode
        ];
    }

    public function buildArray()
    {
        return [
            'project_id'   => $this->projectId,
            'type_id'      => ExternalData::TYPE_PHARMACY,
            'drug_code'    => $this->drugCode,
            'usage_date'   => $this->usageDate,
            'patient_code' => $this->patientCode,
            'data'         => json_encode($this->toInfo()),
            'created_at'   => time()
        ];
    }

}
