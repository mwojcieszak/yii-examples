<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProjectProcessSearch represents the model behind the search form about `app\models\ProjectProcess`.
 */
class ProjectProcessSearch extends ProjectProcess
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'project_id', 'created_at', 'updated_at'], 'integer'],
            [['drug_name', 'drug_code', 'quantity', 'feeding_start', 'feeding_interval'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProjectProcess::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'project_id' => $this->project_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'drug_name', $this->drug_name])
            ->andFilterWhere(['like', 'drug_code', $this->drug_code])
            ->andFilterWhere(['like', 'quantity', $this->quantity])
            ->andFilterWhere(['like', 'feeding_start', $this->feeding_start])
            ->andFilterWhere(['like', 'feeding_interval', $this->feeding_interval]);

        return $dataProvider;
    }

}
