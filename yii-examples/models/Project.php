<?php

namespace app\models;

use DOMDocument;
use DOMElement;
use Yii;
use lib\behaviors\SoftDeleteBehavior;
use lib\helpers\Archive;
use lib\web\XmlResponseFormatter;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "projects".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Unit $companyId
 * @property Unit $ownerId
 */
class Project extends ActiveRecord
{
    public static function find($archive = false)
    {
        $query = parent::find();

        return $query->where('deleted_at is null')->andWhere(['archive' => $archive]);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function __construct($config = [])
    {
        parent::__construct($config);
        if ($this->isNewRecord && !$this->owner_id) {
            $this->owner_id = Yii::$app->user->identity->getCompany() ? Yii::$app->user->identity->getCompany()->unit_id : null;
        }
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'softDelete' => ['class'     => SoftDeleteBehavior::className(),
                'attribute' => 'deleted_at',
                'timestamp' => time(),
                'safeMode'  => true,
            ],
        ];
    }

    public function rules()
    {
        return [
            [
                'name',
                'required'],
            [[
                'created_at',
                'updated_at',
                'approval_rule_id',
                'company_id',
                'owner_id',
                'company_approver_id',
                'institution_approver_id'],
                'integer'],
            [[
                'name'],
                'string',
                'max' => 64],
            ['lock_date', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'                    => Yii::t('app/project', 'NAME'),
            'approval_rule_id'        => Yii::t('app/project', 'APPROVAL_RULE'),
            'company_id'              => Yii::t('app/unit', 'COMPANY'),
            'owner_id'                => Yii::t('app/unit', 'PROJECT_OWNER'),
            'lock_date'               => Yii::t('app/project', 'INSPECTION_DATE'),
            'company_approver_id'     => Yii::t('app/project', 'COMPANY_APPROVER'),
            'institution_approver_id' => Yii::t('app/project', 'INSTITUTION_APPROVER'),
            'created_at'              => Yii::t('app', 'CREATED_AT'),
            'updated_at'              => Yii::t('app', 'UPDATED_AT'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    public static function getArchivePath($id)
    {
        return UPLOAD_ROOT . DS . str_pad($id, 5, '0', STR_PAD_LEFT) . DS . 'archive.zip';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(Document::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovalRule()
    {
        return $this->hasOne(ProjectApprovalRules::className(), ['id' => 'approval_rule_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Unit::className(), ['id' => 'company_id'])->one();
    }

    public function getOwner()
    {
        return $this->hasOne(Unit::className(), ['id' => 'owner_id'])->one();
    }

    public function getCompanyApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'company_approver_id'])->one();
    }

    public function getInstitutionApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'institution_approver_id'])->one();
    }

    public function canManage()
    {
        if (Yii::$app->user->can('project.admin')) {
            return true;
        }

        if (Yii::$app->user->can('project.view.company')) {
            return Yii::$app->user->identity->getCompany()->unit_id == $this->company_id;
        }

        if (Yii::$app->user->can('project.view.institution')) {
            return Yii::$app->user->identity->getCompany()->unit_id == $this->owner_id;
        }
    }

    public function isLocked()
    {
        return $this->lock_date && $this->company_approver_id && $this->institution_approver_id;
    }

    /*
     * TODO:: to implements a method that will check if all documents are approved (MDX-149)
     */
    public function hasApprovedDocuments()
    {
        return true;
    }

    public function approve(User $approver, $date = false)
    {
        if (!$this->hasApprovedDocuments()) {
            return false;
        }

        if (Yii::$app->authManager->checkAccess($approver->id, 'inspections.approve')) {
            $this->institution_approver_id = $approver->id;
        }

        if (Yii::$app->authManager->checkAccess($approver->id, 'projects.approve')) {
            $this->lock_date           = $date ? $date : date('y-m-d H:i:s');
            $this->company_approver_id = $approver->id;
        }

        return $this->save();
    }

    public function isArchive()
    {
        return $this->archive;
    }

    public function archive()
    {
        $this->storeAsXml();

        if (!Archive::toZip($this->toArchiveInfo()['files'], self::getArchivePath($this->id))) {
            return false;
        }

        $this->archive = true;

        return $this->save();
    }

    public function toInfo()
    {
        return [
            'lock_date'            => $this->lock_date ? date('Y-m-d', strtotime($this->lock_date)) : null,
            'company_approver'     => $this->getCompanyApprover() ? Yii::t('app/project', 'APPROVED_BY_COMPANY', $this->getCompanyApprover()->fullName) : null,
            'institution_approver' => $this->getInstitutionApprover() ? Yii::t('app/project', 'APPROVED_BY_INSTITUTION', $this->getInstitutionApprover()->fullName) : null
        ];
    }

    public function toArchiveInfo()
    {
        $subQ = $this->getDocuments()->select(['MAX(version) AS version', 'id', 'name'])->groupBy('id');

        $documents = $this->getDocuments()->innerJoin(['d' => $subQ], 'd.version=documents.version AND d.id=documents.id')->all();

        foreach ($documents as $document) {
            $files[$document->getArchiveFilePath()] = $document->getFilePath();
        }

        $files[Document::getArchiveRootPath() . DS . $this->getXmlFileName()] = $this->getXmlPath();

        return [
            'files' => $files
        ];
    }

    public function fields()
    {
        return [
            'name',
            'owner' => function () {
            return $this->owner->name;
        },
            'verifier' => function () {
            return $this->company_id ? $this->company->name : null;
        },
            'medicalDocuments' => function () {
            return $this->latestProjectDocuments;
        },
            'verificationDocuments' => function () {
            return $this->latestInspectionDocuments;
        },
            'externals' => function () {
            return $this->externals;
        },
        ];
    }

    public function getLatestDocumentsByType($type)
    {
        $subQuery = $this->getDocuments()->select(['MAX(version) AS version', 'id'])->andWhere(['type' => $type])->groupBy('id');

        return $this->getDocuments()->innerJoin(['d' => $subQuery], 'd.version=documents.version AND d.id=documents.id');
    }

    public function getLatestProjectDocuments()
    {
        return $this->getLatestDocumentsByType(Document::PROJECT_DOCUMENT);
    }

    public function getLatestInspectionDocuments()
    {
        return $this->getLatestDocumentsByType(Document::INSPECTION_DOCUMENT);
    }

    public function getExternals()
    {
        return $this->hasMany(ExternalData::className(), ['project_id' => 'id']);
    }

    public function storeAsXml()
    {
        $dom  = new DOMDocument('1.0', 'utf-8');
        $root = new DOMElement('medixee');

        $dom->appendChild($root);

        $formatter = new XmlResponseFormatter();
        $formatter->buildXml($root, $this);

        return file_put_contents($this->getXmlPath(), $dom->saveXML());
    }

    public function getXmlPath()
    {
        return UPLOAD_ROOT . DS . str_pad($this->id, 5, '0', STR_PAD_LEFT) . DS . $this->getXmlFileName();
    }

    public function getXmlFileName()
    {
        return "project{$this->id}.xml";
    }

}
