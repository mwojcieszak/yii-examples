<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProjectSearch extends Project
{
    private $unit_id;

    public function __construct()
    {
        $this->unit_id = Yii::$app->user->identity->getCompany() ? Yii::$app->user->identity->getCompany()->unit_id : null;
    }

    public function rules()
    {
        return [
            [['name'], 'safe'],
            [['created_at', 'updated_at'], 'integer'],
            ['archive', 'boolean'],
            ['lock_date', 'string'],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        if (isset($params['ProjectSearch']['archive'])) {
            $query = Project::find($params['ProjectSearch']['archive']);
        } else {
            $query = Project::find();
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (Yii::$app->user->can('project.view.company')) {
            $query->andFilterWhere([
                'company_id' => $this->unit_id
            ]);
        }

        if (Yii::$app->user->can('project.view.institution')) {
            $query->andFilterWhere([
                'owner_id' => $this->unit_id
            ]);
        }

        $searchModel = $this->load($params);

        if ($this->lock_date == 1) {
            $query->andWhere('lock_date IS NOT NULL');
        }
        if (!($searchModel && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'archive'    => $this->archive,
            'name'       => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}
