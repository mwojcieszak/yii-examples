<?php

namespace app\models;

use Yii;
use lib\behaviors\SoftDeleteBehavior;
use lib\behaviors\VersionBehavior;
use lib\helpers\FileHasher;
use lib\web\UploadedFile;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "documents".
 *
 * @property integer $id
 * @property integer $version
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $project_id
 * @property string $title
 * @property string $size
 * @property integer $author_id
 * @property string $description
 * @property integer $company_approver_id
 * @property integer $signature_type
 * @property integer $type
 * @property integer $institution_approver_id
 * @property integer $deleted_at
 * @property string $signature
 *
 * @property Comments[] $comments
 * @property Projects $project
 * @property User $companyApprover
 * @property User $institutionApprover
 */
class Document extends ActiveRecord
{
    const ARCHIVE_FILES_DIR   = 'FILES';
    const NO_SIGNATURE        = 0;
    const SIGNED_BY_HAND      = 1;
    const SIGNED_DIGITALLY    = 2;
    const PROJECT_DOCUMENT    = 2;
    const INSPECTION_DOCUMENT = 1;

    static $documentTypes  = [
        1 => "INSPECTION_DOCUMENT",
        2 => "PROJECT_DOCUMENT"
    ];
    static $signatureTypes = [
        self::NO_SIGNATURE     => 'UNSIGNED',
        self::SIGNED_BY_HAND   => 'SIGNED_BY_HAND',
        self::SIGNED_DIGITALLY => 'SIGNED_DIGITALLY',
    ];
    public $deleteType     = 'POST';

    public static function find()
    {
        $query = parent::find();

        return $query->where('deleted_at is null');
    }

    public function __construct($data = [])
    {
        if ($data instanceof UploadedFile) {
            parent::__construct(['name' => $data->name, 'size' => $data->size]);
        } else {
            parent::__construct($data);
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            VersionBehavior::className(),
            [
                'class'      => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'author_id',
                ],
            ],
            'softDelete' => ['class'     => SoftDeleteBehavior::className(),
                'attribute' => 'deleted_at',
                'timestamp' => time(),
                'safeMode'  => true,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'documents';
    }

    public static function getTypeForUpload(User $uploader)
    {
        if (Yii::$app->authManager->checkAccess($uploader->id, 'institution_employee') ||
            Yii::$app->authManager->checkAccess($uploader->id, 'institution_head')) {
            return self::PROJECT_DOCUMENT;
        }

        if (Yii::$app->authManager->checkAccess($uploader->id, 'company_employee') ||
            Yii::$app->authManager->checkAccess($uploader->id, 'company_head')) {
            return self::INSPECTION_DOCUMENT;
        }
    }

    public static function canApproveType($type)
    {
        if (($type == self::PROJECT_DOCUMENT && Yii::$app->user->can('document.project.approve')) ||
            ($type == self::INSPECTION_DOCUMENT && Yii::$app->user->can('document.inspection.approve'))) {
            return true;
        }

        return false;
    }

    public static function canManageType($type)
    {
        if (($type == self::PROJECT_DOCUMENT && Yii::$app->user->can('project.document.upload')) ||
            ($type == self::INSPECTION_DOCUMENT && Yii::$app->user->can('inspection.document.upload'))) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'project_id', 'version'], 'required'],
            [['description'], 'string'],
            [['file'], 'file'],
            [['project_id', 'size', 'author_id', 'company_approver_id', 'institution_approver_id', 'id', 'signature_type', 'type'], 'integer'],
            [['title', 'name'], 'string', 'max' => 256],
            [['created_at', 'updated_at'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => Yii::t('app/document', 'ID'),
            'version'             => Yii::t('app/document', 'VERSION'),
            'name'                => Yii::t('app/document', 'NAME'),
            'created_at'          => Yii::t('app', 'CREATED_AT'),
            'updated_at'          => Yii::t('app', 'UPDATED_AT'),
            'project_id'          => Yii::t('app/document', 'PROJECT'),
            'title'               => Yii::t('app/document', 'TITLE'),
            'size'                => Yii::t('app/document', 'SIZE'),
            'file'                => Yii::t('app/document', 'FILE'),
            'author_id'           => Yii::t('app/document', 'AUTHOR'),
            'company_approver_id' => Yii::t('app/document', 'COMPANY_APPROVER_ID'),
            'description'         => Yii::t('app/document', 'DESCRIPTION'),
            'signature_type'      => Yii::t('app/document', 'SIGNATURE_TYPE'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    public function getUrl()
    {
        return Url::toRoute(['document/download', 'id' => $this->id]);
    }

    public function getThumbnailUrl()
    {
        return '';
    }

    public function getDeleteUrl()
    {
        return Url::toRoute(['document/delete', 'id' => $this->id]);
    }

    public function getArchiveFilePath()
    {
        return self::getArchiveRootPath() . DS . Yii::t('app/document', self::$documentTypes[$this->type]) . DS . $this->id . '-' . $this->name;
    }

    public static function getArchiveRootPath()
    {
        return Yii::t('app/document', self::ARCHIVE_FILES_DIR);
    }

    public function getFileName()
    {
        return str_pad($this->id, 5, '0', STR_PAD_LEFT) . '-' . str_pad($this->version, 5, '0', STR_PAD_LEFT);
    }

    public function getRelativeFilePath()
    {
        return str_pad($this->project_id, 5, '0', STR_PAD_LEFT) . DS . $this->getFileName();
    }

    public function getFilePath()
    {
        return UPLOAD_ROOT . DS . $this->getRelativeFilePath();
    }

    public function getCompanyApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'company_approver_id'])->one();
    }

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id'])->one();
    }

    public function getInstitutionApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'institution_approver_id'])->one();
    }

    public function isApprovedByType($type)
    {
        if (Yii::$app->user->can('document.project.approve')) {
            return $this->company_approver_id;
        }

        if (Yii::$app->user->can('document.inspection.approve')) {
            return $this->institution_approver_id;
        }

        return false;
    }

    public function approve(User $approver)
    {
        if (Yii::$app->authManager->checkAccess($approver->id, 'document.project.approve')) {
            $this->company_approver_id = $approver->id;
        }

        if (Yii::$app->authManager->checkAccess($approver->id, 'document.inspection.approve')) {
            $this->institution_approver_id = $approver->id;
        }

        return $this->save();
    }

    public function getSignatureType()
    {
        return self::$signatureTypes[$this->signature_type];
    }

    /**
     * @var UploadedFile
     */
    public $file;

    public function beforeSave($insert)
    {
        $before = parent::beforeSave($insert);
        if ($insert) {
            $this->file = UploadedFile::getInstance($this, 'file');
            if (empty($this->file)) {
                $this->addError('file', Yii::t('app/document', 'ERROR_UPLOADING_FILE'));

                return false;
            }
            if ($this->file->hasError) {
                $this->addError('file', $this->file->error);

                return false;
            }
            $this->name = $this->file->name;
            $this->size = $this->file->size;
            $this->hash = FileHasher::hashFile($this->file->tempName);
            $this->type = self::getTypeForUpload(Yii::$app->user->identity);
        }

        return $before;
    }

    public function beforeValidate()
    {
        if (empty($this->title)) {
            $this->title = $this->name;
        }

        return parent::beforeValidate();
    }

    public function canBeUploadedIntoInspection()
    {
        return Yii::$app->user->can('inspection.document.upload') && !$this->institution_approver_id;
    }

    public function canBeUploadedIntoProject()
    {
        return Yii::$app->user->can('project.document.upload') && !$this->company_approver_id;
    }

    public function fields()
    {
        return [
            'title',
            'version',
            'author' => function () {
            return $this->author->name;
        },
            'description',
            'creation' => 'created_at',
            'name',
            'filename' => function () {
            return $this->getFileName();
        },
            'size',
            'companyApprover' => function () {
            return $this->company_approver_id ? $this->companyApprover->name : null;
        },
            'institutionApprover' => function () {
            return $this->institution_approver_id ? $this->institutionApprover->name : null;
        },
            'signature' => function () {
            return ['.type' => self::$signatureTypes[$this->signature_type], '#' => $this->signature];
        },
            'hash' => function () {
            return base64_encode($this->hash);
        },
        ];
    }

    public function getCryptoTask()
    {
        return [
            'SigningTask' => [
                'Format'          => 'XAdES-BES',
                'DataEmbedding'   => 'true',
                'SigningTaskItem' => [
                    '.Id'               => 'ID-' . sha1($this->id) . '-' . time(),
                    'DataToSign'        => [
                        'URI' => Url::to(['document/xml', 'id' => $this->id], true),
                    ],
                    'SignatureToCreate' => [
                        'URI' => Url::to(['document/signature', 'id' => $this->id], true),
                    ],
                ],
            ],
        ];
    }

}
