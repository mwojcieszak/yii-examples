<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CommentSearch represents the model behind the search form about `app\models\Comment`.
 */
class CommentSearch extends Comment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'project_id', 'document_id', 'author_id'], 'integer'],
            [['content', 'info_attachment', 'info_page', 'info_paragraph'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Comment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'project_id'  => $this->project_id,
            'document_id' => $this->document_id,
            'author_id'   => $this->author_id,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'info_attachment', $this->info_attachment])
            ->andFilterWhere(['like', 'info_page', $this->info_page])
            ->andFilterWhere(['like', 'info_paragraph', $this->info_paragraph]);

        return $dataProvider;
    }

}
