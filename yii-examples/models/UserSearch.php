<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'created_at',
                    'updated_at',
                ],
                'integer',
            ],
            [
                [
                    'name',
                    'last_name',
                    'username',
                    'email',
                    'password',
                    'auth_key',
                ],
                'safe',
            ],
        ];
    }

    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'hidden' => false,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'         => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ]);

        $query->andFilterWhere([
                'like',
                'name',
                $this->name,
            ])
            ->andFilterWhere([
                'like',
                'last_name',
                $this->last_name,
            ])
            ->andFilterWhere([
                'like',
                'username',
                $this->username,
            ])
            ->andFilterWhere([
                'like',
                'email',
                $this->email,
            ])
            ->andFilterWhere([
                'like',
                'password',
                $this->password,
            ])
            ->andFilterWhere([
                'like',
                'auth_key',
                $this->auth_key,
        ]);

        return $dataProvider;
    }

}
