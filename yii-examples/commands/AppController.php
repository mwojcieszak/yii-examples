<?php

namespace app\commands;

use Yii;
use app\models\Project;
use app\models\Unit;
use app\models\User;
use yii\base\Exception;
use yii\console\Controller;

class AppController extends Controller
{
    /**
     * AuthManager
     * @var \lib\rbac\DbManager
     */
    public $auth;

    /**
     * Rule definitions
     * @var array
     */
    private $rules = [
        '\lib\rbac\ProjectRule',
    ];

    /**
     * Permission definitions
     * @var array
     */
    private $permissions = [
        'dev' => [
            'defaultRule' => '\lib\rbac\ProjectRule',
        ],
        'login-without-assignment',
        'adm', 'kli', 'bad',
        'projects', 'projects.approve', 'project.view.company', 'project.view.institution', 'project.rules', 'project.update', 'project.create', 'project.delete', 'project.process.view', 'project.manage.process', 'project.archive.view', 'project.archive.add',
        'units', 'units.business', 'units.medical',
        'edi',
        'permissions', 'roles', 'users',
        'inspections', 'inspections.approve', 'inspections.locked', 'archive',
        'documents', 'document.upload', 'project.document.upload', 'inspection.document.upload', 'document.project.approve', 'document.inspection.approve', 'project.document.delete', 'document.inspection.delete',
        'external-data.view', 'external-data.create',
        'api.token.manage'
    ];

    /**
     * Role definitions
     * @var array
     */
    private $roles = [
        '*',
        'administrator'        => [
            'perms' => ['adm', 'login-without-assignment', 'users', 'edi', 'units', 'api.token.manage'],
        ],
        'institution_employee' => [
            'perms' => [
                'bad',
                'archive',
                'inspections',
                'inspections.locked',
                'projects',
                'project.view.institution',
                'project.rules',
                'project.create',
                'project.update',
                'project.delete',
                'project.process.view',
                'project.manage.process',
                'project.document.upload',
                'project.archive.view',
                'documents',
                'document.upload',
                'document.inspection.approve',
                'external-data.view',
                'external-data.create',
            ],
        ],
        'institution_head'     => [
            'perms' => [
                'bad',
                'archive',
                'projects',
                'inspections',
                'inspections.approve',
                'inspections.locked',
                'project.view.institution',
                'project.rules',
                'project.create',
                'project.update',
                'project.delete',
                'project.process.view',
                'project.manage.process',
                'project.archive.view',
                'project.archive.add',
                'project.document.upload',
                'documents',
                'document.upload',
                'document.inspection.approve',
                'external-data.view',
                'external-data.create',
            ],
        ],
        'company_employee'     => [
            'perms' => [
                'kli',
                'archive',
                'inspections',
                'inspection.document.upload',
                'projects',
                'project.view.company',
                'project.archive.view',
                'project.process.view',
                'documents',
                'document.upload',
                'document.project.approve',
            ],
        ],
        'company_head'         => [
            'perms' => [
                'kli',
                'archive',
                'inspections',
                'inspection.document.upload',
                'projects',
                'projects.approve',
                'project.view.company',
                'project.archive.view',
                'project.process.view',
                'documents',
                'document.upload',
                'document.project.approve',
            ],
        ]
    ];

    /**
     * Unit definitions
     * @var array
     */
    private $units = [
        'Weryfikator S.A.' => [
            'type_id'     => 1,
            'street'      => 'Kontrolerska',
            'city'        => 'Warszawa',
            'postal_code' => '00-000',
            'nip'         => '9876543210',
            'regon'       => '123456789',
        ],
        'Medicus ZOZ'      => [
            'type_id'     => 2,
            'street'      => 'Lekarska',
            'city'        => 'Warszawa',
            'postal_code' => '00-000',
            'nip'         => '1234567890',
            'regon'       => '987654321',
        ],
    ];

    /**
     * User definitions
     * @var array
     */
    private $users = [
        'dev' => [
            'roles'  => [
                '*',
            ],
            'hidden' => true,
        ],
        'b'   => [
            'roles' => [
                'institution_employee',
            ],
            'units' => ['Medicus ZOZ'],
        ],
        'bh'  => [
            'roles' => [
                'institution_head',
            ],
            'units' => ['Medicus ZOZ'],
        ],
        'k'   => [
            'roles' => [
                'company_employee',
            ],
            'units' => ['Weryfikator S.A.'],
        ],
        'kh'  => [
            'roles' => [
                'company_head',
            ],
            'units' => ['Weryfikator S.A.'],
        ],
        'a'   => [
            'roles' => [
                'administrator',
            ],
        ],
    ];

    private function addRules()
    {
        foreach ($this->rules as $rule) {
            if (is_scalar($rule)) {
                $this->rules[$rule] = new $rule();

                $this->auth->add($this->rules[$rule]);
            }
        }
    }

    private function addPermissions()
    {
        foreach ($this->permissions as $key => $perm) {
            if (is_scalar($perm)) {
                $this->permissions[$perm] = $this->auth->createPermission($perm);

                $this->auth->add($this->permissions[$perm]);
            } elseif (is_array($perm)) {
                $this->permissions[$key] = $this->auth->createPermission($key);

                if (isset($perm['defaultRule'])) {
                    $this->permissions[$key]->ruleName = $this->rules[$perm['defaultRule']]->name;
                }

                $this->auth->add($this->permissions[$key]);
            }
        }
    }

    private function addRoles()
    {
        foreach ($this->roles as $key => $role) {
            if (is_scalar($role)) {
                $this->roles[$role] = $this->auth->createRole($role);

                $this->auth->add($this->roles[$role]);
            } elseif (is_array($role)) {
                $this->roles[$key] = $this->auth->createRole($key);

                if (isset($role['defaultRule'])) {
                    $this->roles[$key]->ruleName = $this->rules[$role['defaultRule']]->name;
                }

                $this->auth->add($this->roles[$key]);

                if (isset($role['perms'])) {
                    foreach ($role['perms'] as $perm) {
                        $this->auth->addChild($this->roles[$key], $this->permissions[$perm]);
                    }
                }
            }
        }
    }

    private function addUsers()
    {
        foreach ($this->users as $key => $user) {
            if (is_scalar($user)) {
                $this->users[$user] = $this->replaceUser($user);
                echo "{$this->users[$user]}";
            } elseif (is_array($user)) {
                $this->users[$key] = $this->replaceUser($key, isset($user['roles']) ? $user['roles'] : [], isset($user['hidden']) ? $user['hidden'] : false);
                echo "{$this->users[$key]}";

                $units = $this->users[$key]->addToUnits(isset($user['units']) ? $user['units'] : []);
                if (!empty($units)) {
                    echo "\t" . implode(',', $units);
                }
            }
            echo"\n";
        }
    }

    public function beforeAction($action)
    {
        $this->auth = Yii::$app->authManager;

        return parent::beforeAction($action);
    }

    private function addUnits()
    {
        foreach ($this->units as $name => $data) {
            $unit       = new Unit($data);
            $unit->name = $name;
            if (!$unit->save()) {
                throw new Exception(print_r($unit->errors, true));
            };
            $this->units[$name] = $unit;
            echo "{$unit}\n";
        }
    }

    public function actionReset()
    {
        $this->auth->removeAll();
        User::deleteAll();
        Project::deleteAll();
        Unit::deleteAll();
        $this->actionInit();
    }

    public function actionInit()
    {
        $this->addRules();
        $this->addPermissions();
        $this->addRoles();
        $this->addUnits();
        $this->addUsers();
        $this->addProjects();
    }

    private function replaceUser($name, array $roles = [], $superAdmin = false)
    {
        $existingUsers = User::find()
            ->where(['name' => $name])
            ->orWhere(['username' => $name])
            ->orWhere(['email' => $name . '@evojam.com'])
            ->all();
        if ($existingUsers) {
            foreach ($existingUsers as $u) {
                $u->delete();
                echo "- $u\n";
            }
        }

        $newUser = new User();
        $newUser->load([
            'User' => [
                'name'      => ucfirst($name),
                'last_name' => ucfirst($name),
                'username'  => $name,
                'email'     => $name . '@evojam.com',
                'password'  => $name,
                'roles'     => count($roles) ? $roles : [$name],
            ],
        ]);
        if ($superAdmin) {
            $newUser->access_token = $name;
            $newUser->hidden       = 1;
        }
        if (!$newUser->save()) {
            throw new Exception(print_r($newUser->errors, true));
        };

        echo "+";

        return $newUser;
    }

    private $projects = [
        'one'   => [
            'owner_id'   => 'Medicus ZOZ',
            'company_id' => 'Weryfikator S.A.',
        ],
        'two'   => [
            'owner_id'   => 'Medicus ZOZ',
            'company_id' => 'Weryfikator S.A.',
        ],
        'three' => [
            'owner_id'   => 'Medicus ZOZ',
            'company_id' => 'Weryfikator S.A.',
        ],
    ];

    private function addProjects()
    {
        foreach ($this->projects as $name => $data) {
            if (is_string($data['owner_id'])) {
                $data['owner_id'] = $this->units[$data['owner_id']]->id;
            }
            if (is_string($data['company_id'])) {
                $data['company_id'] = $this->units[$data['company_id']]->id;
            }
            $project       = new Project($data);
            $project->name = $name;
            if (!$project->save()) {
                throw new Exception(print_r($project->errors, true));
            };
            $this->projects[$name] = $project;
            echo "{$project}\n";
        }
    }

}
