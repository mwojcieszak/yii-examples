<?php

return [
    #Menu
    'MENU_COMPANY'                => 'Firmy weryfikujące',
    'MENU_DOCUMENT'               => 'Dokumenty',
    'MENU_INFO'                   => 'Informacje',
    'MENU_INFO_PROJECT'           => 'O projekcie',
    'MENU_INFO_US'                => 'O nas',
    'MENU_INSPECTION'             => 'Kontrole',
    'MENU_LOGIN'                  => 'Zaloguj',
    'MENU_LOGOUT'                 => 'Wyloguj ({0})',
    'MENU_NOTIFICATION'           => 'Wiadomości ({0})',
    'MENU_PERMISSIONS'            => 'Uprawnienia',
    'MENU_PROJECT'                => 'Badania',
    'MENU_PROJECT_APPROVAL_RULES' => 'Reguły zatwierdzania badań',
    'MENU_RESEARCH_UNIT'          => 'Placówki badawcze',
    'MENU_ROLES'                  => 'Role',
    'MENU_USERS'                  => 'Użytkownicy',
    'MENU_ARCHIVE'                => 'Archiwum',
    'NAME'                        => 'Medixee',
    #Permission
    'Add role permissions'        => 'Przypisz uprawnienia dla tej roli',
    'MENU_ADMIN'                  => 'Admin',
    'PERMISSION'                  => 'Uprawnienie',
    'ROLE'                        => 'Rola',
    'ROLE_DATA'                   => 'Dane',
    'ROLE_DESCRIPTION'            => 'Opis',
    'ROLE_NAME'                   => 'Nazwa',
    'ROLE_RULE_NAME'              => 'Nazwa roli',
    'ROLE_TYPE'                   => 'Typ',
    'role_permission'             => 'Uprawnienia roli',
    #General
    'ADD_ELEMENT'                 => 'Dodaj pozycje',
    'ADD_NEW'                     => 'Dodaj',
    'BACK'                        => 'Powrót',
    'CANCEL'                      => 'Anuluj',
    'CREATE'                      => 'Zapisz',
    'CREATED_AT'                  => 'Data utworzenia',
    'DELETE'                      => 'Usuń',
    'DELETE_CONFIRMATION_MESSAGE' => 'Czy napewno chcesz usunąc ten element?',
    'ARCHIVE_CONFIRMATION'        => 'Czy napewno chcesz dodać to badanie do archiwum?',
    'EDIT'                        => 'Edytuj',
    'NO'                          => 'Nie',
    'OBJECT_CANNOT_BE_DELETED'    => 'Obiekt nie może zostać usunięty',
    'OBJECT_CREATED'              => 'Obiekt został poprawnie utworzony',
    'OBJECT_VALUE_CANNOT_BE_SET'  => 'Wystąpił błąd podczas zapisu',
    'SAVE'                        => 'Zapisz',
    'SEND'                        => 'Wyślij',
    'UPDATE'                      => 'Zapisz',
    'UPDATED_AT'                  => 'Data ostatniej edycji',
    'YES'                         => 'Tak',
    'LOGIN'                       => 'Zaloguj',
    'DOWNLOAD'                    => 'Pobierz',
    'NOT_YET_IMPLEMENTED'         => 'Jeszcze nie zaimplementowane',
    #Crypto Applet 'Szafir'
    'LOAD_APPLET'                 => 'Włącz',
    'SIGNATURE'                   => 'Podpis',
    'SIGN'                        => 'Podpisz',
];
