<?php

return [
    'company_employee'     => 'Pracownik firmy weryfikującej',
    'company_head'         => 'Szef firmy weryfikującej',
    'doctor'               => 'Lekarz',
    'institution_employee' => 'Pracownik placówki badawczej',
    'institution_head'     => 'Dyrektor placówki badawczej',
];
