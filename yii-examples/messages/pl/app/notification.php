<?php

return [
    'TITLE'                                  => 'Tytuł',
    'CONTENT'                                => 'Treść',
    'HELLO'                                  => 'Witaj',
    'NEW_NOTIFICATION_MAIL_HEADER'           => 'W systemie pojawiła się nowa wiadomość',
    'NEW_SYSTEM_NOTIFICATION'                => 'Nowe powiadomienie na Twoim koncie',
    'NEW_USER_ACCOUNT_NOTIFICATION_CONTENT'  => 'W systemie zostało utworzone nowe konto użytkownika ({0})',
    'NEW_USER_ACCOUNT_NOTIFICATION_TITLE'    => 'Nowy użytkownik został zarejestrowany',
    'NEW_DOCUMENT_NOTIFICATION_CONTENT'      => 'Dodano nowy dokument do badania {0}',
    'NEW_DOCUMENT_NOTIFICATION_TITLE'        => 'Nowy dokument',
    'NEW_COMMENT_NOTIFICATION_CONTENT'       => 'Dodano nowy komentarz do dokumentu {0}',
    'NEW_COMMENT_NOTIFICATION_TITLE'         => 'Nowy komentarz',
    'DOCUMENT_APPROVED_NOTIFICATION_CONTENT' => 'Dokument ({url}) został zatwierdzony',
    'DOCUMENT_APPROVED_NOTIFICATION_TITLE'   => 'Zatwierdzony dokument',
    'PROJECT_APPROVED_NOTIFICATION_CONTENT'  => 'Badanie ({url}) zostało {msg}',
    'PROJECT_APPROVED_NOTIFICATION_TITLE'    => 'Zatwierdzone badanie',
    'NOTIFICATION_LIST'                      => 'Lista powiadomień',
    'REGARDS'                                => 'Pozdrawiamy',
];
