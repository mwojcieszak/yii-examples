<?php

return [
    'CHOOSE_EMPLOYEES'      => 'Wybierz pracownika',
    'CITY'                  => 'Miasto',
    'COMPANY'               => 'Firma weryfikująca',
    'CREATE_UNIT'           => 'Dodaj',
    'EMPLOYEE'              => 'Pracownik',
    'EMPLOYEES'             => 'Pracownicy',
    'EMPLOYEES_LIST'        => 'Lista pracowników',
    'EMPLOYEE_NOT_ASSIGNED' => 'Nie jesteś pracownikiem żadnej firmy',
    'EMPLOYER'              => 'Pracodawca',
    'EMPTY_EMPLOYEES_LIST'  => 'Brak pracowników do przypisania',
    'LONG_DESCRIPTION'      => 'Opis rozszerzony',
    'NAME'                  => 'Nazwa',
    'NIP'                   => 'Nip',
    'POSTAL_CODE'           => 'Kod pocztowy',
    'PROJECT_OWNER'         => 'Właściciel badania',
    'REGON'                 => 'Regon',
    'RESEARCH_UNIT'         => 'Placówka badawcza',
    'SHORT_DESCRIPTION'     => 'Opis podstawowy',
    'STREET'                => 'Ulica',
    'TYPE_ID'               => 'Typ',
];
