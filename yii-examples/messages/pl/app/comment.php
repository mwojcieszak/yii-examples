<?php

return [
    'ADD_COMMENT'     => 'Dodaj komentarz',
    'AUTHOR'          => 'Autor',
    'COMMENT_LIST'    => 'Lista komentarzy',
    'CONTENT'         => 'Treść',
    'DOC_VERSION'     => 'Do wersji',
    'INFO_ATTACHMENT' => 'Nr załącznika (opcjonalnie)',
    'INFO_PAGE'       => 'Strona dokumentu (opcjonalnie)',
    'INFO_PARAGRAPH'  => 'Paragraf/Akapit (opcjonalnie)',
];
