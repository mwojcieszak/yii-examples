<?php

return [
    'ADDITIONAL_APPROVER'        => 'Dodatkowa osoba zatwierdzająca',
    'APPROVAL_COUNT'             => 'Wymagana liczba zatwierdzeń',
    'APPROVAL_RULE'              => 'Reguła zatwierdzania badania',
    'APPROVAL_RULES'             => 'Reguły zatwierdzania badania',
    'APPROVED_BY_COMPANY'        => 'zatwierdzone przez pracownika firmy weryfikującej {0}',
    'APPROVED_BY_INSTITUTION'    => 'zatwierdzone przez pracownika placówki badawczej {0}',
    'APPROVE_PROJECT'            => 'Zatwierdź badanie',
    'ASSIGN_PROJECT_MEMBERS'     => 'Przypisz użytkowników do badania: {0}',
    'CHOOSE_PROJECT_MEMBERS'     => 'Wybierz członów projktu',
    'COMPANY_APPROVER'           => 'Firma weryfikująca',
    'CREATE_APPROVAL_RULE'       => 'Dodaj regułe zatwierdzania badania',
    'CREATE_PROJECT'             => 'Dodaj badanie',
    'CREATE_PROJECT_PROCESS'     => 'Dodaj element procesu badania',
    'DESCRIPTION'                => 'Opis',
    'DOCUMENT_LIST'              => 'Lista dokumentów',
    'DRUG_CODE'                  => 'Kod leku',
    'DRUG_NAME'                  => 'Nazwa leku',
    'DRUG_QUANTITY'              => 'Dawka',
    'FEEDING_DATE'               => 'Start podawania',
    'FEEDING_INTERVAL'           => 'Interwał (h)',
    'INSPECTIONS'                => 'Kontrole',
    'INSPECTION_DATE'            => 'Data kontroli',
    'INSPECTION_DATE_IS_SET'     => 'Data kontoli została ustawiona na {0}',
    'INSTITUTION_APPROVER'       => 'Placówka badawcza',
    'LOCK_DATE'                  => 'Kontrola',
    'MAIN_APPROVER'              => 'Główna osoba zatwierdzająca',
    'MEMBER_ID'                  => 'Członek badania',
    'NAME'                       => 'Nazwa badania',
    'PROJECT'                    => 'Badanie',
    'PROJECTS'                   => 'Badania',
    'PROJECT_CANNOT_BE_APPROVED' => 'Badanie nie może zostać zatwierdzone',
    'PROJECT_ID'                 => 'Badanie',
    'PROJECT_IS_ALREADY_LOCKED'  => 'Badnie zostało juz zaakceptowane',
    'PROJECT_PROCESS'            => 'Proces badania',
    'RULE_NAME'                  => 'Nazwa',
    'SET-LOCK-DATE'              => 'Ustaw datę kontoli',
    'SET_PROCESS'                => 'Ustaw proces badania',
    'UPDATE_APPROVAL_RULE'       => 'Aktualizuj dane reguły',
    'UPDATE_PROJECT'             => 'Aktualizuj dane badania',
    'UPDATE_PROJECT_PROCESS'     => 'Aktualizuj element procesu badania',
    'INVOICE_HEADER'             => 'Dane finansowo-księgowe',
    'PERSONNEL_HEADER'           => 'Systemy kadrowo płacowe',
    'PHARMACY_HEADER'            => 'Systemy apteki szpitalnej',
    'DOCUMENTATION_HEADER'       => 'System administracji dokumentacji',
    'DOCUMENTATION_NAME'         => 'Nazwa dokumentu',
    'DOCUMENTATION_LINK'         => 'Link do dokumentu',
    'DOCUMENTATION_TYPE'         => 'Typ dokumentu',
    'DOCUMENTATION_DATE'         => 'Data utworzenia',
    'INVOICE_NAME'               => 'Nazwa',
    'INVOICE_NUMBER'             => 'Numer faktury',
    'INVOICE_LINK'               => 'Link do faktury',
    'INVOICE_PRICE'              => 'Wartość bez podatku (PLN)',
    'INVOICE_TAX'                => 'Podatek (%)',
    'PERSONNEL_FULLNAME'         => 'Imię i nazwisko',
    'PERSONNEL_PROCEWITHTAX'     => 'Wynagrodzenie brutto (PLN)',
    'PERSONNEL_LINK'             => 'Link',
    'PHARMACY_DRUG_CODE'         => 'Kod leku',
    'PHARMACY_DRUG_NAME'         => 'Nazwa leku',
    'PHARMACY_USAGE_DATE'        => 'Data użycia',
    'PHARMACY_PATIENT_CODE'      => 'Kod pacjenta',
    'EXTERNAL_DATA'              => 'Dane z systemów zewnętrznych',
    'ARCHIVE'                    => 'Archiwum',
    'PROJECT_ARCHIVED_SUCCESS'   => 'Badanie zostało dodane do archiwum',
    'PROJECT_ARCHIVED_ERROR'     => 'Wystapił błąd podczas archiwizacji badania',
    'ADD_TO_ARCHIVE'             => 'Dodaj do archiwum',
    'GET_XML'                    => 'Pobierz XML',
    'GET_EDI'                    => 'Zobacz EDI',
    'NOTE'                       => 'Notatka',
    'INTO_PROCESS'               => 'Czy jest w procesie badania',
    'VALID_PROCESS'              => 'Poprawny proces',
    'INVALID_PROCESS'            => 'Niepoprawny proces'
];
