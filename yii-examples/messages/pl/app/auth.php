<?php

return [
    'ACCOUNT_LOCKED'                    => 'Konto jest nieaktywne',
    'ADD_ROLE_PERMISSION'               => 'Przypisz uprawnienia dla tej roli',
    'CREATE_PERMISSION'                 => 'Dodaj uprawnienie',
    'CREATE_ROLE'                       => 'Dodaj rolę',
    'CREATE_USER'                       => 'Dodaj użytkownika',
    'EMAIL'                             => 'Adres email',
    'ENTER_NEW_PASSWORD'                => 'Wprowadź nowe hasło',
    'FULL_NAME'                         => 'Imię i nazwisko',
    'INCORRECT_USERNAME_OR_PASSWORD'    => 'Niepoprawny login lub hasło',
    'LASTNAME'                          => 'Nazwisko',
    'LOCK'                              => 'Blokada',
    'LOGIN'                             => 'Zaloguj',
    'NAME'                              => 'Imię',
    'PASSWORD'                          => 'Hasło',
    'PASSWORD_RESET'                    => 'Zresetuj swoje hasło',
    'PASSWORD_RESET_EMAIL_TITLE'        => 'Przypomnienie hasła w serwisie {0}',
    'PASSWORD_RESET_SUCCESS'            => 'Hasło zostało zresetowane poprawnie. Od tej pory możesz uzywać nowego hasła',
    'PERMISSION'                        => 'Uprawnienie',
    'PERMISSIONS'                       => 'Uprawnienia',
    'PERMISSION_DESCRIPTION'            => 'Opis',
    'PERMISSION_NAME'                   => 'Nazwa',
    'PERMISSION_TYPE'                   => 'Typ',
    'REMEMBER_ME'                       => 'Zapamiętaj mnie',
    'RESET_IT'                          => 'zresetuj je',
    'RESET_PASSWORD'                    => 'Zresetuj swoje hasło',
    'RESET_PASSWORD_HINT'               => 'Na podany adres email zostanie wysłany link resetujący hasło',
    'RESET_PASSWORD_TOKEN_SEND_SUCCESS' => 'Aby zrestartować hasło kliknij w link, który znajduje się w przesłanej wiadomości email',
    'ROLE'                              => 'Rola',
    'ROLES'                             => 'Role',
    'ROLE_DESCRIPTION'                  => 'Opis',
    'ROLE_NAME'                         => 'Nazwa',
    'ROLE_PERMISSION'                   => 'Uprawnienia roli',
    'ROLE_TYPE'                         => 'Typ',
    'UPDATE USER'                       => 'Aktualizuj użytkownika',
    'UPDATE_PERMISSION'                 => 'Aktualizuj uprawnienie',
    'UPDATE_ROLE'                       => 'Aktualzuj rolę',
    'USERNAME'                          => 'Nazwa użytkownika',
    'USERS'                             => 'Użytkownicy',
    'USER_DELETED'                      => 'Użytkownik został poprawnie usunięty',
    'USER_EXIST'                        => 'Użytkownik o podanym adresie email istnieje już w systemie',
    'USER_NOT_EXIST_FOR_EMAIL'          => 'Użytkownik o podanym adresie email nie istnieje w systemie',
    'USER_PASSWORD_RESET {url}'         => 'Jesli zapomniałeś hasła, {url}',
    'BAD_TOKEN'                         => 'Token resetu hasła nie istnieje lub stracił swoją ważność',
    'ACCESS_TOKEN'                      => 'Token API',
    'REGENERATE_API_KEY'                => 'Wygeneruj nowy klucz API',
    'API_KEY_REGENERATE_SUCCESS'        => 'Klucz API został wygenerowany poprawnie',
    'API_KEY_REGENERATE_ERROR'          => 'Wystapił błąd podczas generowania klucza API',
];
