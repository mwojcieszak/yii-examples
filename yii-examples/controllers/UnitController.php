<?php

namespace app\controllers;

use Yii;
use app\models\Unit;
use app\models\UnitEmployee;
use app\models\UnitSearch;
use app\models\User;
use lib\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * UnitController implements the CRUD actions for Unit model.
 */
class UnitController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' =>
                        [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'employees-list',
                            'employees-add'
                        ],
                        'roles'   => [
                            'units', 'units.business', 'units.medical',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Unit models.
     * @return mixed
     */
    public function actionIndex(array $UnitSearch)
    {
        $searchModel  = new UnitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ((int) $UnitSearch['type_id'] <= 0) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
                'type_id'      => $UnitSearch['type_id'],
                'title'        => $UnitSearch['type_id'] == 1 ? Yii::t('app/unit', 'COMPANY') : Yii::t('app/unit', 'RESEARCH_UNIT')
        ]);
    }

    /**
     * Displays a single Unit model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Unit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type_id = null)
    {
        $model = new Unit(['type_id' => $type_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'OBJECT_CREATED'));

            return $this->redirect(['index', 'UnitSearch[type_id]' => $type_id]);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Unit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    public function actionEmployeesList($id)
    {
        return $this->redirect(['unit-employee/index', 'UnitEmployeeSearch' => ['unit_id' => $id]]);
    }

    public function actionEmployeesAdd($id)
    {
        $unit = $this->findModel($id);

        $model = new UnitEmployee();

        $model->employee_id = $unit->getEmployesIds();

        $employees = User::getEmployees($unit->type_id);

        if (!$employees) {
            Yii::$app->session->setFlash('danger', Yii::t('app/unit', 'EMPTY_EMPLOYEES_LIST'));

            return $this->redirect(['unit-employee/index', 'UnitEmployeeSearch' => ['unit_id' => $id]]);
        }

        if (Yii::$app->request->getIsPost()) {
            $params = Yii::$app->request->post();

            if (!isset($params['UnitEmployee'])) {
                Yii::$app->session->setFlash('danger', Yii::t('app/unit', 'EMPTY_EMPLOYEES_LIST'));
            } else {
                $unit->saveEmployees($params['UnitEmployee']);
            }

            return $this->redirect(['unit-employee/index', 'UnitEmployeeSearch' => ['unit_id' => $id]]);
        }

        return $this->render('employee-list', [
                'model'     => $model,
                'employees' => $employees,
                'type_id'   => $unit->type_id,
                'id'        => $id
        ]);
    }

    /**
     * Deletes an existing Unit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @param  integer $type_id
     * @return mixed
     */
    public function actionDelete($id, $type_id = null)
    {
        try {
            $model = $this->findModel($id);
            $model->delete();
        } catch (yii\db\IntegrityException $e) {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));
        }

        return $this->redirect(['unit/index', 'UnitSearch' => ['type_id' => $model->type_id]]);
    }

    /**
     * Finds the Unit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer               $id
     * @return Unit                  the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Unit::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
