<?php

namespace app\controllers;

use Yii;
use app\models\Notification;
use app\models\ResetPasswordForm;
use app\models\ResetPasswordRequestForm;
use app\models\User;
use app\models\UserSearch;
use lib\filters\AccessRule;
use lib\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'      => AccessControl::className(),
                'ruleConfig' => ['class' => AccessRule::className()],
                'rules'      => [
                    [
                        'allow' => true,
                        'roles' => [
                            'users',
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'index',
                            'view',
                        ],
                        'roles'   => [
                            'user.view',
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'regenerate-api-key',
                        ],
                        'roles'   => ['api.token.manage'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'request-password-reset',
                            'reset-password',
                        ],
                        'roles'   => ['?'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
        ]);
    }

    /**
     * Displays a single User model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (!$model->canBeEditedOrViewed()) {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));

            return $this->redirect(['index']);
        }

        return $this->render('view', [
                'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $notification = new Notification('notice');

            $notification->add(Notification::NOTIFICATION_EXTERNAL, [
                'title'   => Yii::t('app/notification', 'NEW_USER_ACCOUNT_NOTIFICATION_TITLE'),
                'content' => Yii::t('app/notification', 'NEW_USER_ACCOUNT_NOTIFICATION_CONTENT', $model->email),
                ], [
                'permission' => 'administrator',
            ]);
            Yii::$app->session->setFlash('success', Yii::t('app', 'OBJECT_CREATED'));

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->canBeEditedOrViewed()) {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));

            return $this->redirect(['index']);
        }

        $model->roles = $model->getRolesInfo();

        if ($id == Yii::$app->user->id && !Yii::$app->user->can('account.edit.own')) {
            throw new \Exception('You cannot edit your own account');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'OBJECT_CREATED'));

            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    public function actionRegenerateApiKey($id)
    {
        $model = $this->findModel($id);

        if (!$model->regenerateApiKey()) {
            Yii::$app->session->setFlash('danger', Yii::t('app/auth', 'API_KEY_REGENERATE_ERROR'));
        }

        Yii::$app->session->setFlash('success', Yii::t('app/auth', 'API_KEY_REGENERATE_SUCCESS'));

        return $this->redirect(['index']);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);

            if (!$model->canBeRemoved()) {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));

                return $this->redirect(['index']);
            }
            $model->roles = $model->getRolesInfo();

            $model->delete();
        } catch (yii\db\IntegrityException $e) {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));
        }

        Yii::$app->session->setFlash('success', Yii::t('app/auth', 'USER_DELETED'));

        return $this->redirect(['index']);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);

            if (!$model->user) {
                Yii::$app->session->setFlash('danger', Yii::t('app/auth', 'BAD_TOKEN'));

                return $this->goHome();
            }
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('app/auth', 'PASSWORD_RESET_SUCCESS'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
                'model' => $model,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new ResetPasswordRequestForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('app/auth', 'RESET_PASSWORD_TOKEN_SEND_SUCCESS'));

                return $this->goHome();
            }
        }

        return $this->render('requestPasswordResetToken', [
                'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer               $id
     * @return User                  the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
