<?php

namespace app\controllers;

use Yii;
use app\models\Document;
use app\models\Notification;
use app\models\Project;
use app\models\ProjectMembers;
use app\models\ProjectSearch;
use lib\web\Controller;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProjectController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'create',
                            'update',
                            'delete',
                            'view',
                            'index',
                            'member-list',
                            'inspection',
                            'applet',
                        ],
                        'roles'   => [
                            'project.view.institution',
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'view',
                            'index',
                            'inspection',
                            'applet',
                        ],
                        'roles'   => [
                            'project.view.company',
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'approve-project'
                        ],
                        'roles'   => [
                            'projects.approve',
                            'inspections.approve'
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'archive',
                            'download'
                        ],
                        'roles'   => [
                            'project.archive.view'
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'add-to-archive'
                        ],
                        'roles'   => [
                            'project.archive.add'
                        ]
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'get-info', 'get-edi',
                        ],
                        'roles'   => [
                            'project.archive.view',
                            'edi'
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'edi',
                        ],
                        'roles'   => [
                            'edi'
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'process',
                        ],
                        'roles'   => [
                            'project.process.view'
                        ],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
        ]);
    }

    public function actionEdi()
    {
        $searchModel = new ProjectSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('edi', [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
        ]);
    }

    public function actionInspection()
    {
        $searchModel = new ProjectSearch();

        $params = Yii::$app->request->getQueryParams();

        $dataProvider = $searchModel->search($params);

        return $this->render('inspection', [
                'dataProvider' => $dataProvider,
                'searchModel'  => $searchModel,
        ]);
    }

    public function actionArchive()
    {
        $searchModel = new ProjectSearch();

        $params = Yii::$app->request->getQueryParams();

        return $this->render('archive', [
                'searchModel'  => $searchModel,
                'dataProvider' => $searchModel->search(['ProjectSearch' => array_merge($params, ['archive' => true])]),
        ]);
    }

    public function actionDownload($id)
    {
        $file = Project::getArchivePath($id);

        if (!file_exists($file)) {
            Yii::$app->session->setFlash('danger', Yii::t('app/document', 'ARCHIVE_FILE_NOT_EXISTS'));

            return $this->redirect(['project/archive']);
        }

        return Yii::$app->response->sendFile($file, basename($file));
    }

    public function actionAddToArchive($id)
    {
        $model = $this->findModel($id);

        if ($model->isArchive()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->archive()) {
            Yii::$app->session->setFlash('success', Yii::t('app/project', 'PROJECT_ARCHIVED_SUCCESS'));
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app/project', 'PROJECT_ARCHIVED_ERROR'));

            return $this->redirect(['project/index']);
        }

        return $this->redirect(['project/archive']);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'OBJECT_CREATED'));

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Displays a single Project model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id, $type = false)
    {
        $model = $this->findModel($id);

        if (!$model->canManage()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $subQuery        = $model->getDocuments()->select(['MAX(version) AS version', 'id'])->andWhere(['type' => $type])->groupBy('id');
        $docDataProvider = new ActiveDataProvider([
            'query'      => $model->getDocuments()->innerJoin(['d' => $subQuery], 'd.version=documents.version AND d.id=documents.id'),
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('view', [
                'type'          => $type,
                'model'         => $model,
                'documentModel' => new Document(['project_id' => $id, 'type' => $type]),
                'documentList'  => $docDataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!$model->canManage()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view',
                    'id' => $model->id]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        try {
            $model = $this->findModel($id);

            if (!$model->canManage()) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }

            $model->delete();
        } catch (yii\db\IntegrityException $e) {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'OBJECT_CANNOT_BE_DELETED'));
        }

        return $this->redirect(['index']);
    }

    public function actionApproveProject($id)
    {
        $model = $this->findModel($id);

        if ($model->isLocked()) {
            Yii::$app->session->setFlash('danger', Yii::t('app/project', 'PROJECT_IS_ALREADY_LOCKED'));

            return $this->redirect(['project/inspection']);
        }

        if (!$model->approve(Yii::$app->user->identity)) {
            Yii::$app->session->setFlash('danger', Yii::t('app/project', 'PROJECT_CANNOT_BE_APPROVED'));
        }

        if ($model->institution_approver_id) {
            $info = $model->toInfo()['institution_approver'];
        } else {
            Yii::$app->session->setFlash('success', Yii::t('app/project', 'INSPECTION_DATE_IS_SET', $model->lock_date));
            $info = $model->toInfo()['company_approver'];
        }

        $url = ['project/inspection'];

        $notification = new Notification('notice');
        $notification->add(Notification::NOTIFICATION_EXTERNAL, [
            'title'   => Yii::t('app/notification', 'PROJECT_APPROVED_NOTIFICATION_TITLE'),
            'content' => Yii::t('app/notification', 'PROJECT_APPROVED_NOTIFICATION_CONTENT', ['url' => Url::to($url, true), 'msg' => $info]),
            ], [
            'permission' => 'projects',
            'unit'       => [$model->owner_id, $model->company_id],
        ]);

        return $this->redirect($url);
    }

    public function actionProcess($id)
    {
        return $this->redirect(['project-process/index', 'ProjectProcessSearch' => ['project_id' => $id]]);
    }

    public function actionMemberList($id)
    {
        $model             = new ProjectMembers();
        $model->project_id = $id;

        $model->member_id = $model->getMemberIds();

        if (Yii::$app->request->getIsPost()) {
            $params = Yii::$app->request->post();

            $model->saveMembers($params['ProjectMembers']);

            return $this->redirect(['index']);
        }

        return $this->render('member-list', [
                'model' => $model,
                'id'    => $id
        ]);
    }

    public function actionGetInfo($id, $download = true)
    {
        $project = $this->findModel($id);
        if (!$project) {
            throw new NotFoundHttpException();
        }
        Yii::$app->response->format = Response::FORMAT_XML;

        if ($download) {
            Yii::$app->response->setDownloadHeaders($project->getXmlFileName(), 'text/xml');
        }

        return $project;
    }

    public function actionGetEdi($id)
    {
        return $this->actionGetInfo($id, false);
    }

    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
