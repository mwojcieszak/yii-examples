<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use app\models\Document;
use app\models\DocumentSearch;
use app\models\Notification;
use lib\web\Controller;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'signature', 'xml',
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['documents', 'document.upload'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'approve-document'
                        ],
                        'roles'   => [
                            'document.approve'
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => [
                            'delete'
                        ],
                        'roles'   => [
                            'document.delete'
                        ],
                    ],
                ]
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDownload($id, $version = null)
    {
        $doc = $this->findModel($id, $version);
        if (!$doc) {
            throw new NotFoundHttpException();
        }

        return Yii::$app->response->sendFile($doc->getFilePath(), $doc->name);
    }

    /**
     * Lists all Document models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new DocumentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id, $version = null)
    {
        $doc = $this->findModel($id, $version);

        return $this->render('view', [
                'model'           => $doc,
                'documentModel'   => new Document([
                    'id'         => $doc->id,
                    'project_id' => $doc->project_id,
                    'title'      => $doc->title,
                    'version'    => $doc->version + 1,
                    ]),
                'commentList'     => new ActiveDataProvider([
                    'query'      => $doc->getComments(),
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    ]),
                'newComment'      => new Comment([
                    'document_id' => $doc->id,
                    'project_id'  => $doc->project_id,
                    'doc_version' => $doc->version,
                    ]),
                'previousVerions' => new ActiveDataProvider([
                    'query'      => Document::find()->orderBy('version DESC')->where(['id' => $id]),
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    ]),
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Document();

        if ($model->load(Yii::$app->request->post()) && $model->save() && $model->file->saveAs($model->getFilePath())) {
            $url          = [
                'project/view',
                'id'   => $model->project_id,
                'type' => $model->type,
            ];
            $notification = new Notification('notice');
            $notification->add(Notification::NOTIFICATION_EXTERNAL, [
                'title'   => Yii::t('app/notification', 'NEW_DOCUMENT_NOTIFICATION_TITLE'),
                'content' => Yii::t('app/notification', 'NEW_DOCUMENT_NOTIFICATION_CONTENT', Url::to($url, true)),
                ], [
                'permission' => 'projects',
                'unit'       => [$model->project->owner_id, $model->project->company_id],
            ]);

            return $this->redirect($url);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id, $version)
    {
        $model = $this->findModel($id, $version);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id, 'version' => $model->version]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id, $version)
    {
        $this->findModel($id, $version)->delete();

        return $this->redirect(['index']);
    }

    public function actionApproveDocument($id, $version)
    {
        $model = $this->findModel($id, $version);

        if ($model->isApprovedByType($model->type)) {
            Yii::$app->session->setFlash('danger', Yii::t('app/document', 'DOCUMENT_APPROVED'));

            return $this->redirect(['project/view', 'id' => $model->project_id, 'type' => $model->type]);
        }

        $url = ['project/view', 'id' => $model->project_id, 'type' => $model->type];
        if ($model->approve(Yii::$app->user->identity)) {
            $notification = new Notification('notice');
            $notification->add(Notification::NOTIFICATION_EXTERNAL, [
                'title'   => Yii::t('app/notification', 'DOCUMENT_APPROVED_NOTIFICATION_TITLE'),
                'content' => Yii::t('app/notification', 'DOCUMENT_APPROVED_NOTIFICATION_CONTENT', Url::to($url, true)),
                ], [
                'permission' => 'projects',
                'unit'       => [$model->project->owner_id, $model->project->company_id],
            ]);

            Yii::$app->session->setFlash('success', Yii::t('app/document', 'DOCUEMENT_HAS_BEEN_APPROVED'));
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app/document', 'DOCUMENT_CANNOT_BE_APPROVED'));
        }

        return $this->redirect($url);
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer               $id
     * @return Document              the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id, $version = null)
    {
        if ($version === null) {
            $version = Document::find()->select([new Expression('MAX(version) AS mv')])->where(['id' => $id])->scalar();
        }
        if ($version !== false && ($model = Document::findOne(['id' => $id, 'version' => $version])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionXml($id)
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        return $this->findModel($id);
    }

    public function actionTask($id)
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'TaskList';

        return $this->findModel($id)->getCryptoTask();
    }

    public function actionSignature($id)
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        $model = $this->findModel($id);

        $model->signature = Yii::$app->request->post('signature');

        if (!$model->signature) {
            Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'error';
            throw new BadRequestHttpException();
        }

        $model->signature_type = Document::SIGNED_DIGITALLY;
        if (!$model->save()) {
            Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'error';

            return $model->errors;
        }

        Yii::$app->response->format = Response::FORMAT_RAW;

        return 'OK';
    }

}
