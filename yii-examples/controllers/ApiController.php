<?php

namespace app\controllers;

use Yii;
use app\models\Api\DocumentationInput;
use app\models\Api\InvoiceInput;
use app\models\Api\PersonnelInput;
use app\models\Api\PharmacyInput;
use app\models\ExternalData;
use app\models\Project;
use lib\web\Controller;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\web\Response;

class ApiController extends Controller
{
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'authenticator' => [
                'class'       => CompositeAuth::className(),
                'authMethods' => [
                    HttpBasicAuth::className(),
                    HttpBearerAuth::className(),
                    QueryParamAuth::className(),
                ],
            ],
            'access'        => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['edi'],
                        'verbs'   => ['GET'],
                        'roles'   => [
                            'external-data.view',
                        ],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['invoice', 'personnel', 'pharmacy', 'documentation'],
                        'verbs'   => ['POST'],
                        'roles'   => [
                            'external-data.create',
                        ],
                    ],
                ]
            ],
        ];
    }

    public function actionInvoice($projectId)
    {
        $project = $this->isProject($projectId);

        if (!$project) {
            return Yii::$app->response->setStatusCode(404);
        }

        $invoice = new InvoiceInput($projectId);
        $invoice->load(['InvoiceInput' => (array) json_decode(Yii::$app->request->getRawBody())]);

        $invoice->validate();

        if ($invoice->hasErrors()) {
            return Yii::$app->response->setStatusCode(400);
        }

        $this->save($invoice->buildArray());

        return Yii::$app->response->setStatusCode(201);
    }

    public function actionPersonnel($projectId)
    {
        $project = $this->isProject($projectId);

        if (!$project) {
            return Yii::$app->response->setStatusCode(404);
        }

        $personnel = new PersonnelInput($projectId);
        $personnel->load(['PersonnelInput' => (array) json_decode(Yii::$app->request->getRawBody())]);

        $personnel->validate();

        if ($personnel->hasErrors()) {
            return Yii::$app->response->setStatusCode(400);
        }

        $this->save($personnel->buildArray());

        return Yii::$app->response->setStatusCode(201);
    }

    public function actionPharmacy($projectId)
    {
        $project = $this->isProject($projectId);

        if (!$project) {
            return Yii::$app->response->setStatusCode(404);
        }

        $pharmacy = new PharmacyInput($projectId);

        $pharmacy->load(['PharmacyInput' => (array) json_decode(Yii::$app->request->getRawBody())]);

        $pharmacy->validate();

        if ($pharmacy->hasErrors()) {
            return Yii::$app->response->setStatusCode(400);
        }

        $this->save($pharmacy->buildArray());

        return Yii::$app->response->setStatusCode(201);
    }

    public function actionDocumentation($projectId)
    {
        $project = $this->isProject($projectId);

        if (!$project) {
            return Yii::$app->response->setStatusCode(404);
        }

        $documentation = new DocumentationInput($projectId);

        $documentation->load(['DocumentationInput' => (array) json_decode(Yii::$app->request->getRawBody())]);

        $documentation->validate();

        if ($documentation->hasErrors()) {
            return Yii::$app->response->setStatusCode(400);
        }

        $this->save($documentation->buildArray());

        return Yii::$app->response->setStatusCode(201);
    }

    public function actionEdi($projectId)
    {
        $project = $this->isProject($projectId);

        if (!$project) {
            return Yii::$app->response->setStatusCode(404);
        }

        Yii::$app->response->format = Response::FORMAT_XML;

        return Yii::$app->response->setData($project);
    }

    private function isProject($projectId)
    {
        $project = Project::find()->andWhere(['id' => $projectId])->one();

        if (!$project || !$project->canManage()) {
            return false;
        }

        return $project;
    }

    private function save($data)
    {
        $externalData = new ExternalData();

        if ($externalData->load(['ExternalData' => $data])) {
            return $externalData->save();
        }

        return false;
    }

}
