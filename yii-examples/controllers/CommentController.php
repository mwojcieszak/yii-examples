<?php

namespace app\controllers;

use Yii;
use app\models\Comment;
use app\models\CommentSearch;
use app\models\Notification;
use lib\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => [
                            'signature', 'xml',
                        ],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Comment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new CommentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Comment model.
     * @param  integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
                'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Comment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Comment();

        if ($model->load(Yii::$app->request->post()) && ($model->author_id = Yii::$app->user->id) && $model->save()) {
            $url          = ['document/view',
                'id'      => $model->document_id,
                'version' => $model->doc_version,
            ];
            $notification = new Notification('notice');
            $notification->add(Notification::NOTIFICATION_EXTERNAL, [
                'title'   => Yii::t('app/notification', 'NEW_COMMENT_NOTIFICATION_TITLE'),
                'content' => Yii::t('app/notification', 'NEW_COMMENT_NOTIFICATION_CONTENT', Url::to($url, true)),
                ], [
                'permission' => 'projects',
                'unit'       => [$model->project->owner_id, $model->project->company_id],
            ]);

            return $this->redirect($url);
        } else {
            return $this->render('create', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Comment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['document/view', 'id' => $model->document_id, 'version' => $model->doc_version]);
        } else {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Comment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param  integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param  integer               $id
     * @return Comment               the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionXml($id)
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        return $this->findModel($id);
    }

    public function actionTask($id)
    {
        Yii::$app->response->format = Response::FORMAT_XML;

        Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'TaskList';

        return $this->findModel($id)->getCryptoTask();
    }

    public function actionSignature($id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;

        $comment = $this->findModel($id);

        $comment->signature = Yii::$app->request->post('signature');

        if (!$comment->signature) {
            Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'error';
            throw new BadRequestHttpException();
        }
        if (!$comment->save()) {
            Yii::$app->response->formatters[Response::FORMAT_XML]['rootTag'] = 'error';

            return $comment->errors;
        }

        return 'OK';
    }

}
